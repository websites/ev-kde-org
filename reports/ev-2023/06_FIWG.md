2023 was a year of steady progress for KDE e.V., as we continued to align our
financial strategies with our mission. We closely followed our budget plan, and
are pleased to see the results reflect a disciplined approach to our finances.

Our annual income for 2023 came in just slightly below our projections, with a
small difference of less than 500€. Our expenses came in below target due to a
conservative planning approach that generally assumes an expense maximum, but in
line with our general expectations.

Both outcomes demonstrate our ability to make accurate forecasts — an important
ability given our 2023 operational goal of outspending our annual income for the
second year in a row, which we have achieved, in order to reduce our financial
reserve (which remains high from large one-time donations in earlier years) in
accordance with our non-profit organizational form. However, this must be done
with caution, and we have to carefully track, plan and manage our deficit to
remain sustainable.

A notable highlight on the income side was a large improvement in our Supporting
Membership program, which saw increased participation after moving to Donorbox
and was supported by the successful year-end fundraiser. Additionally, Akademy
2023 set a new sponsorship record, but this was offset by its organizational
costs. This result argues for a watchful eye in keeping our flagship event
sustainable in the future.

Further on the expenses side, our total costs for 2023 were higher than in
previous years, driven primarily by personnel expenses and event-related costs.

Breaking down the figures, corporate support and individual donations continued
to be significant sources of income, with individual donations seeing particular
growth. However, revenue from some of our traditional sources, such as Google’s
Summer of Code, showed a slight decline. On the expense side, personnel costs
remained the largest category, alongside investments in infrastructure and event
organization.

As we move into 2024, our budget plan is focused on maintaining the current
level of activity without major expansions. We aim to increase our income
further while keeping expense growth under control, with a goal of reducing our
reserve burn rate by 18% or better. Our long-term objective is to achieve a
balanced budget (break even) within several years, in time to preserve a good
reserve, and ensuring the sustainability of the organization.

The first months of 2024 have been in line with our expectations. We
successfully organized the conf.kde.in event for the first time in multiple
years, and while Akademy sponsorships is performing lower than in the previous
year, this was not unexpected in a generally difficult year for conferences. The
dissolution of the US-based satellite organization KDE League, anticipated for
several years, has come to pass and will eventually result in the disbursement
of its remaining funds to KDE e.V.

We have also continued to refine our financial tools, particularly our dashboard
application. It now offers better tracking and projection capabilities for our
fundraising campaigns. These improvements are helping us to manage our finances
more effectively and plan for the future with greater confidence.

In conclusion, 2023 was a year of measured growth and careful planning for KDE
e.V. As we look ahead, our focus remains on ensuring that our financial
strategies support our mission and the ongoing work of the KDE community.

<table width="100%">
    <tr>
        <td style="vertical-align: top;">
            <table class="table">
                <thead>
                    <tr>
                        <th><h3>Income (€):</h3></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Patrons/Corporates:</td>
                        <td class="text-right">80,219.74</td>
                    </tr>
                    <tr>
                        <td>Supp. members & donations:</td>
                        <td class="text-right">181.564.20</td>
                    </tr>
                    <tr>
                        <td>Akademy:</td>
                        <td class="text-right">57,750.05</td>
                    </tr>
                    <tr>
                        <td>Other events:</td>
                        <td class="text-right">9,030.83</td>
                    </tr>
                    <tr>
                        <td>GSoC and Code in:</td>
                        <td class="text-right">5,673.29</td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td class="text-right">15,094.55</td>
                    </tr>
                    <tr>
                        <td><b>Total Income:</b></td>
                        <td class="text-right"><b>349,332.65</b></td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td style="vertical-align: top;">
            <table class="table">
                <thead>
                    <tr>
                        <th><h3>Expenses (€):</h3></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Personnel:</td>
                    <td class="text-right" style="color: red">-317,263.14</td>
                </tr>
                <tr>
                    <td>Akademy:</td>
                    <td class="text-right" style="color: red">-43,129.19</td>
                </tr>
                <tr>
                    <td>Sprints</td>
                    <td class="text-right" style="color: red">-12,883.29</td>
                </tr>
                <tr>
                    <td>Other events:</td>
                    <td class="text-right" style="color: red">-20,549.46</td>
                </tr>
                <tr>
                    <td>Infrastructure:</td>
                    <td class="text-right" style="color: red">-17,778.95</td>
                </tr>
                <tr>
                    <td>Office:</td>
                    <td class="text-right" style="color: red">-7,241.61</td>
                </tr>
                <tr>
                    <td>Taxes and Insurance:</td>
                    <td class="text-right" style="color: red">-22,614.56</td>
                </tr>
                <tr>
                    <td>Other:</td>
                    <td class="text-right" style="color: red">-15,611.11</td>
                </tr>
                <tr>
                    <td><b>Total:</b></td>
                    <td class="text-right" style="color:
red"><b>-457,071.31</b></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<table> <tr> <td> <img src="images/FiWG/fiwg_report_2023_piechart.png"/></td>
</tr> </table>
