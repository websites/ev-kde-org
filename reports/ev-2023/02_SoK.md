For this year’s edition of Season of KDE, 8 participants successfully completed
their projects. Several of the projects push forward the work to achieve [KDE’s
three goals](https://community.kde.org/Goals#Current_goals), namely:

- KDE For All: Boosting Accessibility
- Sustainable Software
- Automate and Systematize Internal Processes

### The Projects

[Mohamed Ibrahim](https://community.kde.org/SoK/2023/StatusReport/Mohamed_Ibrahim)
took on the task of improving the
[KdeEcoTest](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest)
emulation tool. The idea behind KdeEcoTest is to provide a
simple-to-use scripting tool for building Standard Usage Scenario scripts and
then running them when measuring the energy consumption of software. Mohamed
first focused on improving the documentation to install and run the tool, then
made several improvements to add functionalities to the tool.

<div> <video width="99%" autoplay loop muted> <source
src="https://eco.kde.org/blog/videos/blog_ex1_fast.mp4" type="video/mp4">
</video>
</div>

[Nitin Tejuja](https://community.kde.org/SoK/2023/StatusReport/Nitin_Tejuja)
also worked on scripting for energy consumption measurements, but with another
approach using the WebDriver for Appium
[selenium-webdriver-at-spi](https://invent.kde.org/sdk/selenium-webdriver-at-spi
). The advantage of this approach is that the Accessibility framework is also
used so contributors will be adding “good” accessibility names — multiple gains
with one addition! Nitin created a script to test the consumption of the KDE
educational suite [GCompris](https://apps.kde.org/gcompris/).

[Rudraksh Karpe](https://community.kde.org/SoK/2023/StatusReport/Rudraksh_Karpe)
furthered work on preparing KDE applications for Blue Angel eco-certification.
At the moment only [Okular has this
certification](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/),
but Rudraksh continued work on the scripts for GCompris and Kate using the
KdeEcoTest tool. Rudraksh also developed the “[SUS Log
Formatter](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/SUS-Log-Formatter)“
tool to provide an overview of the actions taken from a Standard Usage Scenario
log file.

<div> <video width="99%" autoplay loop muted> <source
src="https://eco.kde.org/blog/videos/GCompris-using-KdeEcoTest-590x590.mp4"
type="video/mp4"> </video>
</div>

[Rishi Kumar](https://community.kde.org/SoK/2023/StatusReport/Rishi_Kumar)
worked on improving the accessibility of the Mastodon client
[Tokodon](https://apps.kde.org/tokodon/) also using the WebDriver for Appium
`selenium-webdriver-at-spi`. Rishi added multiple tests using the Accessibility
framework for various functionalities such as search and offline use and
improved the accessibility of Tokodon’s GUI.

[Theophile
Gilgien](https://community.kde.org/SoK/2023/StatusReport/Theophile_Gilgien)
worked on improvements to [AudioTube](https://apps.kde.org/audiotube/).
AudioTube is a client for YouTube, and Theophile added multiple features such as
removing songs from the history, adding a volume slider in maximized player,
making the back-end for search history more efficient, and much more.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/SoK/Audiotubechanges1.png" />
    </figure>
</div>


[Neelaksh Singh](https://community.kde.org/SoK/2023/StatusReport/Neelaksh_Singh)
setup [Flatpak builds in the Continuous
Integration](https://invent.kde.org/packaging/flatpak-kde-applications) workflow
for KDE applications. Neelaksh built on the foundation laid in last year’s SoK
by continuing automatization for the packaging of multiple apps during Nightly
builds.

[Brannon Aw](https://community.kde.org/SoK/2023/StatusReport/Brannon_Aw)
improved the annotation tools in KDE’s
[Spectacle](https://dot.kde.org/2023/05/04/[https://apps.kde.org/spectacle/).
Brannon simplified the way for the eraser tool and clearing annotations, which
was a tedious task before.

[Ruoqing He](https://community.kde.org/SoK/2023/StatusReport/Ruoqing_He)
improved holiday support in the digital clock widget in
[Plasma](https://kde.org/plasma-desktop/). Ruoqing added a sublabel used to
display holiday events for better support.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/SoK/calendar.png" />
    </figure>
</div>
