### [Advisory Board](https://ev.kde.org/advisoryboard.html)

The [KDE Advisory Board](https://ev.kde.org/advisoryboard.php) is a group of
representatives of KDE e.V.’s patrons and other selected organizations that are
close to KDE’s mission and community. The Advisory Board currently has 16
members, and there is a KDE e.V. working group with community members acting as
direct contacts for our partners.

In 2023, we held four calls with our Advisory Board partners — one each quarter
— to inform the members of the Board, receive feedback, and discuss topics of
common interest. These calls covered various topics, including updates on the
status of products such as Plasma 6, KDE Gear and Frameworks, as well as the
launch of products incorporating KDE technologies.

We also discussed KDE’s Community Goals, Season of KDE, Google Summer of Code,
and changes within the KDE community. Members were briefed on the status of our
major events (Akademy, LAS), sprints, and the events in which KDE participated.

The Advisory Board is a place and a symbol of KDE’s collaboration with other
organizations and communities firmly standing behind the ideals of Free and Open
Source Software.

Its current members are: Blue Systems, Canonical, City of Munich, Debian,
FOSS Nigeria, FSF, FSFE, g10 Code, Kubuntu Focus, OpenUK, OSI, Slimbook, SUSE,
The Document Foundation, Qt Group, and TUXEDO Computers. Additionally, our
patrons and supporters including Google, [GnuPG.com](http://GnuPG.com), MBITION,
KDAB, bayskom, and enioka Haute Couture, participate in these calls.

### [Patrons](https://ev.kde.org/supporting-members/)

**Current patrons:** Blue Systems, Canonical, g10 Code, Google, Kubuntu Focus,
Qt Group, Slimbook, SUSE, TUXEDO Computers, MBition.

**Current supporters:** KDAB, basysKom, and enioka Haute Couture, .

### [Community Partners](https://ev.kde.org/community-partners/)

**Current community partners:** Qt Project, Lyx, and Verein Randa Meetings.
