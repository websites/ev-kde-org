<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/PIM/processing-discussions.jpg" />
    </figure>
</div>

On the first weekend of April, the [PIM
](https://community.kde.org/KDE_PIM)team held the KDE PIM Sprint, where
discussions covered various topics, including:

- decoupling KDAV from KIO
- easing the use of `KCalendarCore::Calendar` for asynchronous code (as it
currently exposes a synchronous API)
- overall streamlining of `KCalendarCore::Calendar`’s API to focus on a more
user-friendly core
- deciding on a new custom properties API for `KCalendarCore`
- retiring the mixed maildir and Kolab resources with careful consideration for minimal disruption to users
- improving Windows compatibility (especially crucial for Kalendar)
- replacing the Kross-based account wizard with a modern QML one
- fixing important dependencies in Akonadi-contacts to reduce reliance on widgets (essential for Kalendar and mobile)
- proper usage of QtKeychain in KMailTransport
- removing the KMailTransportAkonadi API
- reducing Kalendar’s reliance on certain KOrganizer settings
- enhancing APIs for QML consumption
- factoring out some of Kalendar internals for reuse, and planning the overall timeline towards a “PIM 6” release.

The KDEPIM team, currently fairly small, faced constant concerns which
influenced some of the choices made, to keep plans realistic given the large
codebase and limited manpower for maintenance. Full notes on the KDE PIM Sprint
can be found [here](https://community.kde.org/Sprints/PIM/2023).
