We have always seen KDE as a key component in delivering Linux systems that
compete favorably with proprietary computers. In fact, it’s the only desktop
environment we’ve ever shipped.

Customers today want Linux systems that are self-explanatory and work in low- or
no-I.T. environments. They also require easy setup and automatic updates, where
every major change is validated on their device before they see it, even years
after the sale.

Because KDE is so intuitive and welcoming, we can focus on the other components
needed to deliver a polished product and ecosystem that showcases this beautiful
desktop. We’re proud to be a tester, contributor, and patron.

Michael S. Mikowski, Technical Product Manager, [Kubuntu
Focus](https://kfocus.org)
