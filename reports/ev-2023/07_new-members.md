KDE e.V. welcomed the following new members in 2023:

* Bart de Vries
* Joshua Goins
* Andre Heinecke
* James Graham
* Julius Künzel
* Kisaragi Hiu
* Emir Sarı
