<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/GSoC/GSoCPlusKDE.png" />
    </figure>
</div>

KDE successfully mentored seven projects in 2023’s edition of [Google Summer of
Code](https://summerofcode.withgoogle.com/) (GSoC). GSoC is a program in which
contributors new to Free and Open Source software spend between 175 and 350
hours during from 10 to 22 weeks working on an Open Source project. This post
summarises the projects and contributors for 2023 and their achievements.

## Projects

<figure class="image-right"> <img width="80%"
src="images/GSoC/aakarsh_merkuro.png" /></figure>

### [Merkuro](https://apps.kde.org/merkuro.calendar/)

- [Mail integration was improved in
Merkuro](https://community.kde.org/GSoc/2023/StatusReports/AakarshMJ) by
[**Aakarsh MJ**](https://invent.kde.org/aakarshmj). This project made it
possible to compose and send mail via Merkuro.

- [Calendar availability was
implemented](https://community.kde.org/GSoc/2023/StatusReports/AnantVerma) by
[**InfiniteVerma**](https://invent.kde.org/infiniteverma). This allows you to
specify the hours when you are available and can be invited to meetings and
events. This is still work in progress, and hopefully, it will be polished and
merged soon.

### [digiKam](https://www.digikam.org/)

<figure class="image-right"> <img width="80%" src="images/GSoC/gsoc23_TRAN.png"
/></figure>

- [Automatic tags assignment tools were added and the face recognition engine
was improved for
digiKam](https://community.kde.org/GSoc/2023/StatusReports/QuocHungTran)
by [**TRAN Quoc Hung**](https://invent.kde.org/quochungtran), who developed a
deep learning model that can recognize various categories of objects, scenes,
and events in digital photos. The model generates corresponding keywords that
can be stored in digiKam’s database and assigned to photos automatically. Merged
in the DigiKam 8.2.0.

### [Krita](http://krita.org/)

- [The Bundle Creator was
improved](https://community.kde.org/GSoc/2023/StatusReports/SrirupaDatta)
by [**Srirupa Datta**](https://invent.kde.org/srirupa). Bundles are packages of
resources, like brushes or gradients that Krita users can exchange. The work was
merged and is already part of Krita.

### [KDE Eco](https://eco.kde.org/)

- [Measuring energy consumption using remote lab was
implemented](https://community.kde.org/GSoc/2023/StatusReports/KaranjotSingh) by
[**Karanjot Singh**](https://invent.kde.org/drquark). Although the lab is
physically located at the KDAB offices in Berlin, with
[KEcoLab](https://invent.kde.org/teams/eco/remote-eco-lab) it is now accessible
to KDE and other Free Software developers from anywhere in the world. This was
achieved by setting up backend CI/CD integration and automating the energy
measurement process, including providing a summary of the results.

<figure class="image-right"> <img width="80%"
src="images/GSoC/rishi_moderation_tool_incognito.png"
/></figure>

### [Tokodon](https://apps.kde.org/tokodon/)

- [A moderation tool was added in
Tokodon](https://community.kde.org/GSoC/2023/StatusReports/RishiKumar) by
[**Rishi Kumar**](https://invent.kde.org/keys), who worked on implementing
the admin APIs in Tokodon. Rishi added various moderation tools that were all
merged and made available in Tokodon.

### [Okular](https://okular.kde.org/)

- [Okular for Android was
improved](https://community.kde.org/GSoC/2023/StatusReports/ShivoditGill) by
[**Shivodit**](https://invent.kde.org/shivodayt), as they brought in the
much-needed font rendering improvement when fonts are not embedded in the PDF
file ­— text was not being rendered (image on left), and now they are (image on
right). Other improvements were also carried out during the period, such as
improving the “About” page and finding the root cause of a freeze on Android.
All of Shivodit’s was merged into the various repositories.

# Follow-up

Even though the GSoC 2023 period is over, it does not mean the contributions
stopped there. Contributors had had a fun summer honing their skills within KDE
with the community’s support, and many continue to contribute to their projects.
