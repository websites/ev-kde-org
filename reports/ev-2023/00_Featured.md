If there was one topic occupying the KDE community in 2023, it was building
Plasma 6. Now, if you weren’t following the process closely, it might have
seemed like Plasma 6 just… appeared, fully formed like Aphrodite from the waves,
and with surprisingly few blemishes.

Of course, that is not how it happened. Niccolò Venerandi traces the first
public mention of Plasma 6 back to a discussion started in
[2018](https://tube.kockatoo.org/w/uvNes5uvn2oTNSzMuLjw2j), and that discussion
never ended.

While the topic was being discussed in forums, instant messaging channels,
social media, and blogs, developers were working around the clock to adapt
Plasma and KDE applications to a new underlying version of the Qt toolkit. The
combined code bases of all the migrated projects is huge, and not only did a
bunch of volunteer programmers carry out the migration successfully, but they
also found time to improve support for the environment on Wayland, add new
features, and restore [older features that had had to be
removed](https://floss.social/@kde/111335412278767938) for technical reasons.

Not only that, but sunk deep into the core of KDE’s ecosystem is more than twice
as much code users never get to see. These are the foundations of Plasma and
KDE’s applications: [KDE
Frameworks](https://develop.kde.org/products/frameworks/).

The Frameworks provide tools and components that allow front-end developers to
build the things you actually see. Frameworks contribute to the consistent look
of buttons, scrollbars and menus across applications, and help developers
quickly build interfaces and add features. They contribute to the widespread
implementation of novel functionalities, like compact hamburger menus, and
drop-down searches from within apps. They make KDE’s software safer, more
efficient, and more stable across the board, as bug fixes in one framework will
cascade down to all the apps that use it. Frameworks help make more compact
applications — such as instant messaging clients, social media apps and travel
and weather assistants — work equally well on desktop, embedded, and mobile
devices.

KDE’s underlying Frameworks code spills out beyond the borders of our projects,
finding its way into the infotainment systems of cars and buses, gaming
consoles, and embedded control systems. Let’s not forget that, to this day, most
web browsers still contain lines of code written by KDE hackers in the late
1990s in a framework called KHTML, one of the longest living and most ubiquitous
HTML engines in the history of HTML engines. KDE is essentially the grandfather
of the modern web and all apps built using web technologies.

And every single line of the many hundreds of thousands that make up KDE’s
frameworks has been written, revised, modified, and then updated by a human —
often unpaid, usually unthanked, and with no corporate-style bureaucracy or
hierarchical organization, either. Just passionate experts identifying needs and
addressing them, behaving collegially and productively for the sheer joy of it.

So the next time you marvel at the titanic body of work that is KDE’s software
corpus, spare a thought for the frameworkers, the unsung heroes of the Free
Software development world. Few if any of KDE’s applications, Plasma features,
extensions and widgets would be available or as feature-rich without them.
