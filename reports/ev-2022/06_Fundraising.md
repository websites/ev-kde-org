KDE's work is made possible thanks to the contributions from KDE Community members, donors and companies that support us. In 2022, two fundraising campaigns were launched and successfully completed. 

### Kdenlive Fundraiser

The first campaign, the [Kdenlive Fundraiser](https://kdenlive.org/en/fund/), was created to support [Kdenlive](https://kdenlive.org) contributors. The aim was to raise €20,000, and the campaign exceeded expectations and raised over €21,000, and is still drawing in donations.

The money raised will go towards financing the development of new features, like nested timelines, a new effects panel, and making keyframing more powerful. Developers will also be able to improve the overall performance of Kdenlive, making it faster, more responsive, and even more fun to work with.

### End of Year Fundraiser

The second campaign, the [End of Year Fundraiser](https://kde.org/fundraisers/yearend2022/), had a goal of raising €20,000 and also surpaased that figure and reached over €25,000 before the end of year.

The funds raised will support KDE's efforts to continue developing its spectacular Plasma desktop and all the apps you need for education, productivity, and creative work. It will also hep to finance Akademy, other community events and sprints.

The campaign was successful in attracting a large number of donors, who helped KDE continue to be able to offer users, contributors and community members better services and support, develop applications and frameworks, and support hardware platforms.

The success of these fundraising campaigns is a significant milestone for KDE and highlights the importance of fundraising for the community. The funds raised will provide valuable resources for the growth of KDE, its community, its commitment towards its projects and Free Software and how everybody can benefit from it.
