<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/LAS/groupphoto_1500.jpg" alt="LAS 2023 attendees posing for the event's group photo" />
    </figure>
</div>

[Linux App Summit (LAS) ](https://linuxappsummit.org/) was held online and in-person from the 29th to the 30th of April. KDE and GNOME co-hosted the conference that brings the global Linux community together to learn, collaborate, and help grow the Linux application ecosystem.

The event was well attended both in-person and online. For many, LAS 2022 was the first conference they could physically attend after more than two years of webcam communications, and the human need to be present around like-minded people in the flesh was palpable. Invigorated, passionate and optimistic, the open-source enthusiasts from both the GNOME and KDE camps descended on Rovereto for two days of innovation, sharing and fun.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/LAS/opening.jpg" alt="Neill McGovern (GNOME) and Aleix Pol (KDE) on stage at LAS 2022" />
        <br />
        <figcaption>Neill McGovern (GNOME) and Aleix Pol (KDE) open LAS 2022</figcaption>
    </figure>
</div>

The event included talks, panels and Q&As on a wide range of topics, including a talk about "How to Community: Anatomy of a Healthy Open Source Communities and Projects" delivered by Anita Ihuman, Aleix Pol Gonzalez talked about "Your app on the Steamdeck", "Energy Conservation and Energy Efficiency with Free Software" by Joseph De Veaugh-Geiss, and many more.
