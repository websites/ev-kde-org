The KDE community has always had one clear, overarching goal, and it is neatly summarised in our [vision](https://community.kde.org/KDE/Vision):

> "[*To help bring about a*] world in which everyone has control over their digital life and enjoys freedom and privacy."

"Control", "freedom" and "privacy" are tenets of all Free Software projects, but what makes KDE different is the "everyone" bit.

KDE was founded in the mid-nineties, when GNU/Linux was the plaything of mainly computer science students, researchers, and expert developers, as well as senior sysadmins and others with long, unkempt beards.

The original creators of KDE wanted to break GNU/Linux out of that small circle and set about building an environment and a set of apps that would be attractive to and easy to use by end users. That is, the "everyone" in the KDE vision statement.

Despite being clear and unambiguous, KDE's vision is broad and requires breaking down into smaller objectives. That is where KDE's Goals (with a capital "*G*") come in.

### What have Goals ever done for Us?

KDE's Goals are decided every two years. As KDE is a bottom up style organization, that is, the larger community decides which way the project should go, and it is also the larger community that decides what these Goals are going to be. Ideas are submitted, refined, and voted for. Finally, at the end of the process, three goals are approved and announced, usually during KDE's [Akademy](https://akademy.kde.org/) event.

The latest set of goals was announced during [Akademy 2022](https://tube.kockatoo.org/w/dDRqBYhtEv9eMRXevHHVjw) and refocused the Community's targets for the next two years.

We'll be coming to what the new goals are in a minute, but first, it is worth noting that choosing new goals does not mean superseding goals from prior years. KDE's goals have a tendency of working their way into the fabric of how the community works and redefine parameters, priorities and workflows.

Indeed, The [Usability and Productivity](https://community.kde.org/Goals/Usability_%26_Productivity) goal from 2018 has led to a shift in how devs and designers build their apps within KDE, making interfaces easier to understand and trimming out confusing controls and features, for example. This trend will continue for the foreseeable future.

Or take the [All about the Apps](https://community.kde.org/Goals/All_about_the_Apps) goal, which has helped the rest of the world realize that KDE is not only about the Plasma desktop, but has other world class products that are excellent in their own right.

The work that went into the [Wayland](https://community.kde.org/Goals/Wayland) goal has laid the foundations for the next generation of the Plasma environments: whether for desktop computers, mobile devices, smart TVs, or vehicles, it will influence the development of Plasma, KDE apps and frameworks for years, probably decades to come.

### The Shape of Goals to Come

What it does mean is that areas the KDE community see as requiring prioritization will get the attention they need. The goals chosen in 2022 are a prime example of this: [Accessibility](https://community.kde.org/Goals/KDE_For_All) is hard and tedious and hence unpopular with developers working for free. However, improving apps and environments for people who have problems with their eyesight or hearing, or find using a mouse, keyboard or trackpad problematic will open up KDE software to a whole new audience and truly make it for the "everyone" mentioned in our vision. Again KDE works for the future: once the groundwork is laid, KDE devs and designers will not go back to designing software without bearing accessibility in mind.

Something else we cannot ignore is the effects of inefficient, power-devouring, CO<sub>2</sub>-spewing and electronic waste-generating software. Hence the [Sustainability](https://community.kde.org/Goals/Sustainable_Software) goal will spearhead the effort to reduce the impact of KDE software on the environment. Despite its lofty objectives, the community is pretty sure it can pull this off thanks to prior successes even before starting: the team working on the [KDE Eco project](https://eco.kde.org/), which started in 2021, has already [created a laboratory and means to measure the energetic efficiency of software](https://eco.kde.org/blog/2022-05-30-sprint-lab-setup/), and scored a massive goal when [Okular](https://okular.kde.org/), KDE's multi-purpose and feature-rich document reader, became the [first ever officially eco-certified computer program](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/).

Each goal gives KDE a new level of sophistication, a higher bar to jump. We are not only concerned about the software working, we also concerned about it looking good, and being usable, that it works for people with different physical skills, and that we help reduce the impact of digital technology on the environment. With the growing sophistication comes the need to preserve the knowledge we acquired on the way. We cannot afford to lose knowledge of how things are done just because a developer retires.

The ['Automate and Systematize Internal Processes'](https://community.kde.org/Goals/Automate_and_systematize_internal_processes) goal addresses this issue. This goal aims to improve institutional memory and preserve shared knowledge by automating and systematizing internal processes, and does so by expanding documentation, and encouraging people to work more cohesively in teams, sharing information, instead of working alone.

So, again it is easy to see how, despite being set in 2022, and officially running until 2024, the current goals will live on beyond that date and become an integral part of how KDE seeks to accomplish its vision.

---

Read on to find out what the KDE Community got up to during 2022.
