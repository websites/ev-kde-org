<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="75%" src="images/Projects/kodaskanna.png" alt="Screenshot of Kodaskanna with a ascanned QR barcode with Konqi the drago sitting on top." />
    </figure>
</div>

New beta software [Kodaskanna](https://mail.kde.org/pipermail/kde-announce-apps/2022-January/005686.html) is a utility for reading data from 1D/2D barcodes (e.g. QR codes or barcodes) and making the data available for further processing.

The long-term vision for Kodaskanna is to be a simple utility to integrate in workflows where some data processing expects a data blob (or a series of blobs) to be taken from a machine readable source by the user. It should have reusable general purpose extensions, both for reading all kinds of encoded data from all kinds of sources (e.g. graphical, acoustical, etc.), or straight from dedicated input devices. I t should also validate and preview the extracted data in an expected data format. The invoking instance should be able to filter/define what is possible. The utility should be usable both in-process and out-of-process, and ideally, itself be replaceable by other solutions providing the same interface.
