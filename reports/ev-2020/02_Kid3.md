<figure class="image-right"> <img width="50%" src="images/Projects/kid3.png" alt="Kid3" /><figcaption>Kid3.</figcaption></figure>

[Kid3](https://kid3.kde.org/) is a handy but powerful music tagging program that lets you edit the ID3 tags and similar formats on MP3 and other music files. Kid3 has moved to be hosted by KDE and has made its first release as a KDE app.

If you want to easily tag multiple MP3, Ogg/Vorbis, FLAC, MPC, MP4/AAC, MP2, Opus, Speex, TrueAudio, WavPack, WMA, WAV and AIFF files (e.g., full albums) without typing the same information again and again and have control over both ID3v1 and ID3v2 tags, then Kid3 is the program you are looking for.

### Features

With Kid3 you can:

* Edit ID3v1.1 tags.
* Edit all ID3v2.3 and ID3v2.4 frames.
* Convert between ID3v1.1, ID3v2.3 and ID3v2.4 tags.
* Edit tags in MP3, Ogg/Vorbis, DSF, FLAC, MPC, MP4/AAC, MP2, Opus, Speex, TrueAudio, WavPack, WMA, WAV, AIFF files and tracker modules (MOD, S3M, IT, XM).
* Edit tags of multiple files, e.g., the artist, album, year and genre of all files of an album typically have the same values and can be set together.
* Generate tags from filenames.
* Generate tags from the contents of tag fields.
* Generate filenames from tags.
* Rename and create directories from tags.
* Generate playlist files.
* Automatically convert upper and lower case and replace strings.
* Import from gnudb.org, MusicBrainz, Discogs, Amazon and other sources of album data.
* Export tags as CSV, HTML, playlists, Kover XML and other formats.
* Edit synchronized lyrics and event timing codes, import and export LRC files.
* Automate tasks using QML/JavaScript, D-Bus or the command-line interface.

Kid3 is available for [Linux](https://www.linux-apps.com/p/1126630/), Windows, macOS and Android and in the following app stores: [Chocolatey](https://community.chocolatey.org/packages/kid3/), [Homebrew](https://formulae.brew.sh/cask/kid3) and [F-droid](https://f-droid.org/en/packages/net.sourceforge.kid3/).