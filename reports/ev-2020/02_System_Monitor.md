<figure class="image-right"> <img width="50%" src="images/Projects/systemmonitor.png" alt="System Monitor" /><figcaption>System Monitor.</figcaption></figure>

[Plasma System Monitor](https://github.com/KDE/plasma-systemmonitor) is a brand new UI for monitoring system resources. It is built on top of Kirigami and a new system statistics service called *KSystemStats* debuted in Plasma 5.19. It shares a lot of code with the new system monitor applets that were also introduced in Plasma 5.19 and is meant to be a successor to [KSysGuard](<https://apps.kde.org/ksysguard/>). Plasma System Monitor provides an interface for monitoring system sensors, process information and other system resources. It allows extensive customisation of pages, so it can be made to show exactly the data you want to see.

### Features

* On startup, you see a quick overview of your entire system: memory, disk space, network and CPU usage.
* The *Applications* page shows you all running applications along with detailed statistics and graphs for those applications.
* You can select the *Line Chart* display mode for any column that displays a numeric value.
* The *CPU* chart will be displayed stacked by default.
* You can edit panels and divide the page into several different rows, columns and sections.