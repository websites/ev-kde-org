The [OpenUk awards ](https://openuk.uk/openuk-awards-second-edition-2021/)recognise and celebrate the best in open tech in the UK. It was held on 11 November with a day of events about sustainability with technology emphasising why open tech is the most effective way to do that.

Sessions included an opening from former government minister Francis Maude, Launch of the OpenUK Consortium Data Centre Blueprint, Open Collaboration Opening Sustainability led by Red Hat, Opening Up the Energy Sector, building the Sustainable Open Future for the UK.

In the evening I hosted the OpenUK awards 2021, showcasing and recognising the best people and organisations for open tech in the UK. I also made an announcement about KDE’s sustainability effort in front of the politicians and tech audience.
