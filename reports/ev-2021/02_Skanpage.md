[Skanpage](https://apps.kde.org/skanpage/) is KDE's new image scanning app. It's a simple scanning application designed for multi-page scans and saving of documents and images.

It works for scanning from both flatbed and feed-through automatic document feeder scanners. It lets you configure options for the scanning device, such as resolution and margins, and you can re-order, rotate and delete scanned pages. The scans can be saved to multi-page PDF documents and image files.
