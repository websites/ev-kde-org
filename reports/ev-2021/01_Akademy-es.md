<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Supported_Activities/Akademy-ES.jpg" alt="Attendees to Akademy-es 2021" />
    </figure>
</div>

The sixteenth edition of [Akademy-es](https://www.kde-espana.org/akademy-es-2021-en-linea), KDE’s yearly event for the Spanish speaking community, was held online from 19th to 21st November. For over 15 years, Akademy-es has been promoting and bringing this great event to the general public as well as to all the developers.

This year as well, there was a weekend full of virtual talks, presentations, workshops and virtual social events in which we discovered the news of the KDE Community and the progress of one of the most important free projects within Free Software.

The conference started with the opening ceremony by Adrián Chaves, president of KDE Spain. Then there were talks on various topics such as The KDE Qt 5.15 Patch Collection, Kdenlive, 10 KDE tricks, Blockchain and free software, Hardware and KDE, as well as lightning talks and much more.
