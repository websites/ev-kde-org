From 17th May to 31st August 2021, the KDE Community welcomed Google Summer of Code students for 2021. Google Summer of Code (GSoC) is a global program focused on attracting students to Open Source software development. Students work with an Open Source organization on a three-month programming project during their break from school.

This was our 16th year of mentoring students. The following students have worked with our development teams throughout the summer.

* [Harsh Kumar](https://summerofcode.withgoogle.com/archive/2021/projects/6331159855759360) and [Mariam Fahmy](https://summerofcode.withgoogle.com/archive/2021/projects/5199894997893120) worked on GCompris. The goal of the GCompris project this year was to add new activities.
* [Bhumit Attarde worked on Integrating XFSTESTS with KDE KIO-FUSE.](<https://summerofcode.withgoogle.com/archive/2021/projects/6366779563048960>)
* [Sachin Jindal worked on Reference Image Improvement. This project aimed to improve the Reference Image Tool in Krita and include some additional features that will be helpful for the users.](<https://summerofcode.withgoogle.com/archive/2021/projects/6400421773443072>)
* [Suraj Kumar Mahto worked on KMyMoney: Integration of the new functionalities from the Alkimia library.](<https://summerofcode.withgoogle.com/archive/2021/projects/6457717576695808>)
* [Chukwuebuka Ezike worked on adding gdb pretty printer support for Qt5.](<https://summerofcode.withgoogle.com/archive/2021/projects/5091756680413184>)
* [Claudio Cambra worked on Plasma Mobile - Akonadi Calendar. This project aimed to implement a fully-featured convergent calendar application for Plasma Mobile.](<https://summerofcode.withgoogle.com/archive/2021/projects/5123523701374976>)
* [Tanmay Chavan worked on implementing an efficient algorithm for smarter boolean operations on vector shapes in Krita.](<https://summerofcode.withgoogle.com/archive/2021/projects/5579183928901632>)
* [Nghia Duong worked on face engine improvements in digiKam.](<https://summerofcode.withgoogle.com/archive/2021/projects/6512493744095232>)
* [Swapnil Tripathi worked on podcast support in Alligator.](<https://summerofcode.withgoogle.com/archive/2021/projects/6560176739450880>)
* [Anjani Kumar worked on porting digiKam to Qt6 on Linux.](<https://summerofcode.withgoogle.com/archive/2021/projects/6116725258452992>)
* [Santhosh Anguluri worked on collective operations over multiple layers/groups in Krita.](<https://summerofcode.withgoogle.com/archive/2021/projects/5967948631506944>)
* [Valentin Boettcher worked on KStars DSO overhaul.](<https://summerofcode.withgoogle.com/archive/2021/projects/5128799867371520>)
* [Phuoc-Khanh LE worked on improving digiKam Image Quality Sorter algorithms.](<https://summerofcode.withgoogle.com/archive/2021/projects/5139632848633856>)
* [Mahmoud Khalil worked on improving custom/image stamp annotation handling in Okular.](<https://summerofcode.withgoogle.com/archive/2021/projects/5143288469782528>)
