This new KDE application is a convergent contact management application for both desktop and mobile devices. It provides a central place for starting conversations. Depending on the information available about a contact, you can phone, text or message them.

With Phonebook you can

* view contacts
* add and remove contacts
* import vCard files.
