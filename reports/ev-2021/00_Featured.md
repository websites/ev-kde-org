2021 was a significant year for KDE. We made [a special website](https://25years.kde.org/), we designed and sold stickers and T-shirts and dug out historic photos of events and landmarks, as well as screen-grabs of KDE being used in the wild, including at NASA, on TV shows, and behind the scenes during the filming of the likes of James Cameron's Avatar.

We delved deeper still into the history of the project and we put up virtual machine images of older versions of KDE/Plasma so visitors could experience for themselves what it was like to be a KDE user in the 90s and 2000s.

We met people from around the world, put together a video with our friends and played a jolly, bouncy song on a loop in the background for 4 minutes straight. For the icing on the virtual cake, we held special chats, including one with KDE founder Matthias Ettrich. ([All of the above is available from KDE's 25th Anniversary page](https://25years.kde.org/)).

<div align="center">
  <iframe title="In Conversation with Matthias Ettrich, Founder of KDE" src="https://tube.kockatoo.org/videos/embed/fd4cbea3-1e7b-42f1-a856-865085ea88d7" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0">
  </iframe>
</div>

After all, it is not every day the project you started celebrates its 25th anniversary.

#### Once Upon a Time in Linux

Even more so if you consider that Matthias Ettrich's epiphany, when he started KDE back in 1996, was not, [according to his own admission](https://tube.kockatoo.org/w/xh9ZWZNbpfTC2azkQaDQNk), a long term one. He was definitely not thinking of literally thousands of contributors building graphical environments for all sorts of devices and dozens of applications of the most diverse nature.

Matthias Ettrich's vision was much more mundane. He had just come back from a hiatus away from computers, and discovered the promising primordial soup of the 80s microcomputing scene, the world of the Commodore 64s, ZX Spectrums, Amstrad CPC64s, and so many other marvellous and exciting machines, had died. Inexplicably, it had been replaced by the overpriced, clunky, drab-grey and boring desert of the IBM PC and MS DOS.

<figure class="image-right">
  <img width="100%" src="images/IntroFeatured/1997kdeone.jpg" alt="KDE meetup in 1997" />
  <figcaption>Matthias Ettrich (first from right) meets with fellow developers at the KDE 1 event in 1997.</figcaption>
</figure>

Nevertheless, young Matthias still enrolled to study Computer Science, and soon discovered Linux, a system that married the retro of 1970s' command line UNIX tools, with the futuristic of advanced networking and nascent online services.

Charming and exciting as it was, Matthias quickly realized that Linux would have a hard time going mainstream among everyday computer users in its then cryptic and arcane form. So he set to work making Linux friendly for everyone. That idea is what morphed over time into [KDE's current vision](https://community.kde.org/KDE/Vision):

"A world in which everyone has control over their digital life and enjoys freedom and privacy."

### The Day Today in KDE

So, a quarter of a century later, how well has KDE as a community and a project managed to keep to its promise of giving control, providing freedom and privacy-protecting software to a large audience?

KDE has grown beyond the development of a desktop and has become a true environment, in that it provides a fertile space for other projects to mature and prosper. [Krita](https://krita.org/en/), for example has managed to become a de facto yardstick for digital painting program, with more than a million downloads a year. [LabPlot](https://labplot.kde.org/), KDE's tool for data analysis and visualization, has been quietly making its way into research and engineering circles worldwide and is now used in such prestigious institutions as NASA and CERN. [Kdenlive](https://kdenlive.org/), KDE's full-featured video editor, has officially broken into the mainstream and has been accepted into the [ASWF Landscape](https://landscape.aswf.io/), the list backed by the Linux Foundation and the Academy of Motion Picture Arts and Sciences (i.e. the folks that give out the Oscars) of recommended open source tools for movie-making. Kdenlive is quickly becoming one of the favourite editors for indie film-makers, advertisers and vloggers. [GCompris](https://www.gcompris.net/), our suite of fun, colourful and educational activities for children, is so ubiquitous, it is nearly impossible to keep up with all the people and organizations using it. There is not a day that goes by without us discovering a new school, education board, or whole regional or national educational authority that has decided to deploy GCompris to support their teachers.


<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/IntroFeatured/apps.jpg" alt="KDE apps running on Plasma" />
        <br />
        <figcaption>KDE success stories: From left to right, LabPlot, Krita, Kdenlive and GCompris running on the Plasma desktop environment.</figcaption>
    </figure>
</div>

All combined, KDE's apps put the number of KDE software users easily into the many millions, even by the most conservative estimates. It would be fair to conclude that, with that amount of users, KDE is on the right route to becoming a provider of Free Software for everyone.

And then we have KDE's Plasma desktop. Now, a desktop is a hard sell to end users outside the FLOSS bubble. Installing an app is one thing, but installing a whole new graphical environment is another thing altogether. Even Linux diehards will tend to get Plasma bundled with their distro of choice and not go out of their way to install it by hand.

Notwithstanding, Plasma is growing its user base by becoming a versatile platform for hardware vendors. Companies are preinstalling Plasma on their laptops, experimenting with Plasma Mobile on phones and tablets, and looking to Plasma because it allows them to build customised interfaces for smart devices.

For the last few years, we have lived with the sensation that we were teetering on the brink of a major breakthrough into the end user market. One push would bring about the fabled year of the Linux desktop. And the push came.

Through indirect estimates, we could already place the number of Plasma users conservatively in the 15 million region, but, in November 2021, Valve's Steam Deck catapulted Plasma into the hands of eager gamers and, at the same time, into the mainstream. For the first time, a massive potential market opened for a Linux desktop, and gamers will browse the web and their files, install software (through KDE's [Discover](https://apps.kde.org/en-gb/discover/)) and launch programs, run emulators so they can play older games, chat with friends, and check the news, and they will do all this using Plasma. KDE's desktop provides an environment that users will find responsive, attractive, stable and familiar.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/IntroFeatured/kde_deck.jpg" alt="Steam Deck" />
        <br />
        <figcaption>Valve's Steam Deck runs KDE's Plasma for its desktop mode.</figcaption>
    </figure>
</div>

In short, Plasma is on spec and provides an environment for everyone.

### Back to the Future

Where do we go from here? Well, the existing breakout apps, frameworks and environments have still plenty of places to go. Despite their growing popularity, KDE's creativity, productivity, development and educational applications have just started nibbling at the edges of markets that have been dominated up until now by closed source alternatives. Also, KDE is run by a community that has no need for revenue goals in a given timeframe and does not decide the viability of its projects based on the results written into quarterly reports. This means the rules that can often make or break a corporate product do not apply, and a KDE project can patiently wait and gradually work up to the right moment and the right breakthrough.

The same goes for Plasma, an environment that can run virtually anywhere and on anything, and also look like anything, making it what providers would want from an OEM environment: lightweight, ready-made, secure and customisable. The world of consoles, mobile and smart devices is Plasma's oyster.

As for things unimagined, precisely because KDE is run by its community, and the community is varied and imaginative, it is unlikely we will ever run out of new ideas and projects that will continue to contribute to bringing KDE's vision about.

---

Read on to find out what the KDE got up to during its 25th year of existence.
