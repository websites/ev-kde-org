### [Advisory Board](https://ev.kde.org/advisoryboard.html)

The [KDE Advisory Board](https://ev.kde.org/advisoryboard.php) is a group of representatives of KDE e.V.'s patrons and other select organizations that are close to KDE's mission and community. It currently has 13 members, and there is a KDE e.V. working group with community members acting as direct contacts for our partners.

In 2021, we held two calls with our Advisory Board partners, one in January and one in October, with the goal always being to inform the members of the Board, receive feedback and discuss topics of common interest. The calls covered several topics, including updates on the status of Qt and the role of the [KDE Free Qt Foundation](https://kde.org/community/whatiskde/kdefreeqtfoundation/), KDE's Community Goals, and our partnerships with hardware vendors regarding products that are shipping or will be shipped with KDE technologies.

We also discussed changes within the e.V.'s organization, our staff and contractors, and the new job ads we planned to publish to further support the KDE community and its products. Finally, the members were briefed on the status of our major events (Akademy, LAS) and all the sprints that took place remotely.

The Advisory Board is a place and a symbol of KDE’s collaboration with other organizations and communities firmly standing behind the ideals of Free and Open Source Software.

Its current members include Blue Systems, Canonical, City of Munich, Debian, enioka Haute Couture, FOSS Nigeria, FSF, FSFE, OpenUK, OSI, Pine64, Slimbook, SUSE, The Document Foundation, The Qt Company, and TUXEDO Computers.

### [Patrons](https://ev.kde.org/supporting-members.html)

**Current patrons:** Blue Systems, Canonical, enioka Haute Couture, Google, The Qt Company, Slimbook, SUSE, Tuxedo Computers.

**Current supporters:** KDAB, basysKom.

### [Community Partners](https://ev.kde.org/community-partners.html)

**Current community partners:** Qt Project, Lyx and Verein Randa Meetings.
