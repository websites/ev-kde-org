---
title: "KDE e.V. Travel Cost Reimbursement Policy"
layout: page
---

## Eligibility
KDE e.V. can only reimburse costs incurred in pursuit of the goals of KDE e.V.
or for KDE e.V. running its business. A common example is expenses for
travelling to and attending KDE-related events in cases where otherwise you
wouldn't be able to attend, because the costs of doing so are unusually high, or
because paying for it yourself would be a great financial burden. In these
cases, please try to find a 3rd-party sponsor first, e.g. your employer, school,
etc.

The following types of events and reasons for attending are generally eligible
for reimbursement by KDE e.V.:

- Attending a developer sprint or other meeting as a KDE contributor.
- Attending a conference, trade show, or other important meeting and
  representing KDE there.
- Attending Akademy as a speaker, organizer, or KDE contributor.
- Attending Akademy or a work-related event as a KDE e.V. employee,
  contractor, or Board member.
- Attending an in-person meeting of the KDE e.V. Board of Directors as a Board
  member.

Reimbursement is always subject to availability of funds and approval by the KDE
e.V. Board of Directors, and can never be guaranteed.


## What's covered
KDE e.V. can reimburse up to 100% of the transportation and lodging expenses for
the cheapest reasonable trip. This generally means:

- For long distance travel, lowest fare class bus, train, or plane ticket.
- For local travel, local public transit, e.g. metro or bus. Taxi
  transportation can also be considered for reimbursement in extenuating
  circumstances, such as when:
  - No public transit is available.
  - Transporting heavy materials or equipment to an event.
  - Unexpected adverse weather makes walking for extended distances infeasible
    or dangerous.
  - Multiple people sharing a taxi would actually be cheaper than everyone
    individually using public transit.
- Lowest cost single room in a hostel or cheap hotel. A double room shared with
  someone else is preferable, and can be more fun, too! If the hotel offers
  breakfast at an additional daily cost, please pay this cost yourself. To ease
  bookkeeping, this is done on the honor system rather than being strictly
  enforced, but please do keep it in mind.

For KDE e.V.'s employees and contractors attending work events, an exception to
the above rules is that food will also be reimbursed, and both food and lodging
expenses will be reimbursed at the [per diem rates set forth by the government
of Germany (2024 version)](https://www.bundesfinanzministerium.de/Content/DE/Downloads/BMF_Schreiben/Steuerarten/Lohnsteuer/2023-11-21-steuerliche-behandlung-reisekosten-reisekostenverguetungen-2024.pdf?__blob=publicationFile&v=1)
regardless of actual expenses.


## Travel Support Workflow
Two things to note upfront:

1. You must make the purchases yourself using your own funds. Expenses will be
   reimbursed later, after the event concludes. In rare exceptions for
   circumstances of grave financial urgency, KDE e.V. may be able to make the
   purchases for you, at its discretion.
2. Requests for travel reimbursement made without pre-approval by KDE e.V.'s
   Board of Directors using https://reimbursements.kde.org will be automatically
   denied. This includes employees and contractors spending from a defined team
   budget (though in this case there is an expedited review process). All
   requests must go through https://reimbursements.kde.org. **There are no
   exceptions.**

---

### Before the event
Visit https://reimbursements.kde.org/events and then click on the
event you are asking for reimbursement to attend. If it's not listed,
e-mail the KDE e.V. Board at &lt;kde-ev-board@kde.org&gt; and ask for it to be
created.

Once you're on the event page, click the blue _Travel Support_ button. In the
_Description_ field, provide the following information:

1. The purpose of the trip.
2. Who you are and how you're involved in KDE.
3. Why you need a subsidy and why KDE e.V should be the one to provide it.

Employees and contractors can omit #2 and #3.

Then under the _Expenses_ section, add an item for each expense (e.g. plane
ticket, local metro, hotel room). If you've already made the purchases, enter
their exact costs. If not, estimate. **Actual expenses exceeding approved
estimates will require re-approval or else they will be paid only up to the
approved amount**.

Click the blue _Create travel support request_ button at the bottom of the page
to proceed.

If any changes are needed, click the white _Edit_ button at the top. If
everything looks good, click _Action_ at the top and then _Submit_.

At this point, members of the KDE e.V. Board of Directors will approve or deny
the request within a few weeks.

If the request is approved, visit the link in the the email you receive and look
it over. Does everything look good? Do you accept the amounts that the Board has
indicated they'll be willing to reimburse? If so, click _Action_ at the
top and then _Accept_.

If you disagree, click _Action_ and then _Roll Back_. Change whatever needs
changing and then re-submit it. Continue until the request is denied by the
Board or accepted by you.

If your travel support request is accepted, but you cannot attend the event for
reasons outside your control (e.g. illness, transit strike, dangerous weather)
and you are unable to receive a refund for costs you have incurred, KDE e.V.
will reimburse the costs anyway. No reimbursement will be made for failure to
attend an event due to preventable negligence (e.g. poor planning, lack of valid
travel documents).

---

### During the event
Attend and enjoy the event! Remember to collect the receipts for all expenses
for which you're requesting reimbursement.

If actual costs for any items in your reimbursement request end up being higher
than originally expected, these can be made up for being any expenses in the
reimbursement request that ended up being lower than expected. In this case,
roll back the travel support request as described earlier, and change the costs
for each item to reflect reality, making sure not to exceed the previously
approved total. Changes of this nature will be automatically approved.

If you incur any additional unplanned yet reimbursable expenses during the
event, decide whether you'd like to apply for them to be reimbursed as well. If
so, roll back the travel support request as described earlier, add the expenses,
and re-submit it. Such additional expenses are subject to conditional
re-approval and cannot be guaranteed.

---

### After the event
After the event, write a blog post that's visible on https://planet.kde.org
about your experiences! Tell the community what transpired, how you contributed
and what you learned,  and how it helped KDE. This way the wider KDE community
can learn about the experience as well. This must be your own original content;
**blog posts obviously written by AI or containing AI-generated art will not be
accepted**. If you don't yet have a blog syndicated on https://planet.kde.org,
this is a perfect time to start one and
[add it](https://invent.kde.org/websites/planet-kde-org#-planet-kde)!

_The requirement for writing a blog post is waived for KDE e.V. employees and
contractors, but strongly encouraged anyway._

Next go to https://reimbursements.kde.org/travel_sponsorships, find your travel
support request, and click on it. Then click the blue _Request reimbursement_
button.

On that page, paste a link to the blog post you wrote. Then attach receipts **in
the PDF format** for every expense you'd like to be reimbursed for, and enter
the actual amounts of money spent. Expenses without receipts cannot be
reimbursed.

Finally, fill in the financial details for how you'd like to be paid:

- For bank transfers within Europe, enter the name of the account holder, the
  name of the bank, and the IBAN number.
- For bank transfers outside of Europe, enter the name of the account holder,
  the account number, the name of the bank, the bank's BIC/SWIFT code, and the
  bank's postal address.
- For transfers to PayPal accounts, enter the PayPal account's email address.

After all of that, download the acceptance letter, [sign it using Okular](https://pointieststick.com/2023/06/13/tips-tricks-handwritten-signatures-in-okular/),
and re-upload it. Then click _Action_ and _Accept_. The reimbursement will then
be processed and paid.

---

_This policy is maintained by the KDE e.V Board of Directors._
