---
title: "KDE e.V. Community Working Group"
layout: page
---

### Goals

The Community Working Group takes care of KDE's community members. It enables the KDE Community to work effectively and efficiently by helping the community members maintain healthy interactions and resolving conflicts.

The Community Working Group was established at the General Assembly of KDE e.V. in August 2008.

### Rules

> The Community Working Group has no specific rules for joining.
> The team expect the highest level of discretion and privacy for members. The team handles 
> members personal information, and there is a [charter](#charter).

### Members

+ Andy Betts (anditosan@kde.org, irc: anditosan[m], matrix: [@anditosan:kde.org](https://matrix.to/#/@anditosan:kde.org))
+ Jules Enriquez (win8linux@gmail.com, matrix: [@win8linux:kde.org](https://matrix.to/#/@win8linux:kde.org))
+ Kye Potter (annoyingrain5@gmail.com, matrix: [@kyepotta:kde.org](https://matrix.to/#/@kyepotta:kde.org))
+ Neofytos Kolokotronis (neofytosk@kde.org, matrix: [@neofytosk:kde.org](https://matrix.to/#/@neofytosk:kde.org))
+ Noah Davis (noahadvs@gmail.com, matrix: [@noahdvs:kde.org](https://matrix.to/#/@noahdvs:kde.org))
+ Victoria Fischer (tdfischer@hackerbots.net, matrix: [@tdfischer:kde.org](https://matrix.to/#/@tdfischer:kde.org))

### Contact

[community-wg@kde.org](mailto:community-wg@kde.org)

## Charter

As a large and diverse community, KDE benefits from having a trustworthy central point of contact for questions about communication between participants as well as a respected independent mediator between participants and users of KDE.


The Community Working Group aims to act as a central point of contact for members by communicating user needs and concerns to developers, and developer intentions and plans to regular users.


The group also acts as mediator, upon request, in cases where communication among members breaks down. Issues regarding expectations are coordinated with the Marketing Working Group.

#### Goal and strategy

The long-term goal of the Community Working Group is to help to maintain a friendly and welcoming KDE community, thereby ensuring KDE remains a great project enjoyed by all contributors and users.


We use a participative approach. The Community Working Group acts as supporting staff who help where help is needed.
The group is prepared to help community members whenever they request support. For example, in cases where a member becomes targeted or when a member needs help expressing opinions or ideas to the team.


We actively approach members, where we feel it is necessary, to understand specific situations and help.

#### Tasks

Short-term activities already complete include:

+ The creation of [userbase.kde.org](https://userbase.kde.org), a wiki site where documentation helpful to end-users may be placed and maintained either by developers or by other users. UserBase will also be the home for FAQ documents which will provide a resource for both developers and users to help dispel misconceptions, misunderstandings and misinformation regarding the KDE project - useful, for example, in IRC or mailing list discussions.
+ A mailing list, [community-wg@kde.org](mailto:community-wg@kde.org), is in operation, and is the contact point for requests for help of any kind.
+ Assistance in writing a [Code of Conduct](https://www.kde.org/code-of-conduct/) was given, and the CoC was ratified at the General Assembly of the KDE e.V in 2008.


Mid- to long-term activities are:

+ Make people aware of issues that run contrary to social norms and expectations as documented in the KDE
  [Code of Conduct](https://www.kde.org/code-of-conduct/), and requesting to refrain from negative behavior.
+ Help mailing lists administrators, forums, IRC channels and others with edge cases. Mailing lists administrators, the "op" on IRC channels, and other community leaders can request a second opinion from the Community Working Group prior to making their decision. Our team provides additional perspective on specific situations and suggest resolutions.
+ Mediate when the 
  [Code of Conduct](https://www.kde.org/code-of-conduct/) is violated. Our mediation [policy](#policy) is listed in this document.
+ Manage expectations together with the Marketing Working Group.
+ Help prevent burn-out among long time contributors and help to assess options for those who are under stress.
+ Explore  the possibility of  setting up a representation
  council from the user base.


#### Staffing

The inner-circle of the Community Working Group is the mailing list where issues are discussed.  Mail from outside the CWG staff is moderated, but all legitimate mail will be accepted (i.e. spam will be dropped, all mail related to the activities of the CWG will be accepted). Depending on the situation, individuals may be invited to join the mailing list to discuss specific aspects of the CWG's.

When descalation is necessary, the team may hold private conversations about public matters to be sensitive to all involved. This method provides a space for members to feel heard and trusted in their specific community-related situation.

[community-wg@kde.org](mailto:community-wg@kde.org) is a private mailing list. Issues discussed on the list must not be disclosed without permission from the CWG and those involved in any particular issue.
We ask people who wish to stay anonymous to approach a trusted member of the Community Working Group, who will then bring the issue forward to the rest of the Group.

The Community Working Group is made up of people with a track record of dealing with difficult issues and who have qualities relevant for community work.


Current members of the Community Working Group are named on [the e.V. website page about the CWG](https://ev.kde.org/workinggroups/cwg.php).


### Policy for Dealing with Breaches of the Code of Conduct {#policy}

This policy ensures that confidentiality is respected at all times. If circumstances require the entire e.V. to audit a CWG process, the CWG will provide details to the membership.

A private and secure shared webspace has been, or will, be created on a KDE server, accessible only to members of the Community Working Group (CWG). Here a full logging system will be maintained of all contact, of any kind, relating to specific problems.  Included information will be a date-stamp, the name of the CWG member reporting, and a summary of the contact made.  An example of the form is presented as Appendix A.

The information in this log will be maintained for a minimum of 1 year, but may be deleted at a later date, when the CWG consider that there is no further relevance.

If the problem can be resolved amicably no public action will be taken.  Should the problem appear to be intractable the CWG will come to a joint decision as to the form of action that is most helpful for the KDE community at large, and will submit the findings to the e.V. Board. When the e.V. Board is satisfied that all necessary steps have been taken, the decision will be taken to moderators and other interested parties explaining the decision and asking for action to be taken. The CWG leaves the enforcement on recommendations of a given case to moderators.

Following that step the mailing list of the e.V. will be notified of the action taken. At that point members of the e.V. have the right to audit the process, satisfying themselves that policy has been strictly carried out.


At the end of a case, on request, the CWG will prepare a report.  This report will contain:

+ Information to indicate the time-scale of the enquiry
+ Number and type of communications that took place
+ Outcome.

The report does not identify any participant by name other than the subject of the enquiry. A sample of such a report is presented as Appendix B. Requests for any confidential material will be denied.

#### Appendix A - Example of the format of log maintained by the CWG

<p>2009-01-13      nightrose       alerted to possible issue on somelist.kde.org ML<br />
20:05:15</p>
<p>2009-01-15      annew           CWG received email from John Doe requesting help<br />
21:17:01                        regarding behaviour of Jonathon Buck on somelist.kde.org</p>
<p>2009-01-16      toma            email sent to Jonathon Buck requesting moderation<br />
16:40:27                        of language on MLs</p>
<p>2009-01-19      annew           further angry exchanges on somelist.kde.org.  email<br />
17:14:02                        sent to JB outlining some strategies for avoiding<br />
such conflict.</p>
<p>2009-01-20      annew           defensive reply from JB, saying no-one listens to<br />
11:14:21                        him.  email sent to JB explaining that his attitude<br />
over a considerable period have led to this situation.  anger-management strategies stressed.</p>
<p>2009-02-19      annew           similar complaint from James Sow against Jonathon<br />
11:49:32                        Buck's behaviour on #somechannel.</p>
<p>2009-02-19      nightrose       raised a private query with JB and tried to explain<br />
22:03:58                        why his attitude is upsetting people.  JB agreed,<br />
though seeming reluctant, to attempt to moderate his language on both MLs and IRC<br />
etc.</p>

#### Appendix B - Example of the form of report to be released upon request by an e.V. Member

**January '09**<br />
Alert of problem on a ML received by email<br />
List member requests moderation/help<br />
First email sent to JB requesting a more moderate approach to problem.<br />
Three further exchanges of mail on this subject.

**February '09**<br />
Five further exchanges of mail - no noticeable difference in attitude<br />
A new complaint against JB raised by another ML member<br />
Private IRC query with JB attempts to explain why people are offended by his remarks<br />
etc......

**March '09**<br />
15th email sent, third IRC session with JB - no noticeable change.<br />
No further progress seems possible.<br />
CWG confer and conclude that all avenues have been explored<br />
and that it can only recommend a period of suspension.<br />
Recommendation reported to the e.V.
