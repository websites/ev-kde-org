---
title: Careers in KDE
menu:
  main:
    parent: organization
    weight: 11
    name: Careers

# ### NO OPEN POSITIONS
#
# Set *openpositions* to `false`
#
# ### OPEN POSITIONS
#
# The value of *openpositions* is a list. Start each list entry
# with a -, then on each line write the key and value, e.g.
#
#  - title: Chief Borker
#    description: Borks at borbs. Borks at leafs. Protec and be cute as hec.
#
# Valid keys for a list entry are *title* and *description*.
#
# ### NOTES
#
# When updating this list, also update the top-level content/_index.md file
# to indicate whether there are openings.
#
# When adding a job entry, also add a news item at content/news/
#
openpositions:
 - title: Plasma Software Engineer
   description: KDE e.V. seeks a contractor to improve KDE’s Plasma desktop environment in ways that support user acquisition through growth into new hardware and software markets, by addressing defects and missing features that are barriers to these objectives. Please see the [full job listing](/resources/jobad-plasma-software-engineer-2025.pdf) for more details about this opportunity.

---

{{< career >}}

Feel free to contact kde-ev-board<span>@</span>kde.org with any questions.

