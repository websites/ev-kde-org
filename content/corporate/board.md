---
title: People
menu:
  main:
    parent: organization
    name: Board of Directors
    weight: 1

board:
  - name: Aleix Pol i Gonzàlez
    title: President
    email: aleixpol<span>@</span>kde.org
    description: Aleix Pol i Gonz&agrave;lez has been collaborating with KDE since 2007. He started working in
      software development in the KDE Education area and KDevelop.
      Aleix joined the KDE e.V. board of directors in 2014.
      In his day-job, he is employed by MBition GmbH where he has worked with other parts of the community
      including Plasma and Qt.
    image: /corporate/pictures/apol.jpg
    elected: 2023
  - name: Eike Hein
    title: Treasurer and Vice President
    email: hein<span>@</span>kde.org
    description: Eike Hein has been a KDE contributor since 2005. Initially working
      on applications and later on Plasma as developer and designer, he has also served
      on KDE's Sysadmin team and co-authored the <a href="https://manifesto.kde.org/">KDE Manifesto</a>.
      Eike joined the KDE e.V. board of directors in 2017.
    image: /corporate/pictures/eike.jpg
    elected: 2023
  - name: Lydia Pintscher
    title: Vice President
    email: lydia<span>@</span>kde.org
    description: Lydia Pintscher has been with KDE since 2006. She started doing
      marketing, and later community and release management for Amarok. She
      has since moved on to community management and running mentoring
      programs for all of KDE. She is on the board of directors of KDE e.V.
      since 2011. In her day-job she does product management for
      Wikidata at Wikimedia Germany.
    image: /corporate/pictures/lydia.jpg
    elected: 2023
  - name: Nate Graham
    title: Board Member
    email: nate<span>@</span>kde.org
    description: Nate joined KDE in 2017 after his KDE goal "Top-Notch Usability and Productivity for Basic Software" was chosen and he became its goal champion until 2020. As part of this effort, he started a blog to highlight progress on the goal, which persists today as a technical news publication for the KDE community. In 2022 his KDE goal "Automate and Systematize Internal Processes" was chosen and became a goal champion again, and he also joined the Board of Directors. In his day job, he works at Blue Systems as a QA manager for Plasma and internal projects.
    image: /corporate/pictures/nate.jpg
    elected: 2022
  - name: Adriaan de Groot
    title: Board Member
    email: groot<span>@</span>kde.org
    description: Adriaan encountered KDE in 1999 and has been involved in various roles
      ever since. He served on the board of KDE e.V. 2006-2011. In KDE development he participated in KDE PIM,
      FreeBSD porting, Solaris porting, the creation of the English Breakfast
      Network (for code-quality checking) and api.kde.org. He can usually be found in the
      Netherlands and occasionally contributes to Calamares, the independent
      Linux System Installer. He works for a manufacturer of inspection systems
      for pharmaceutical glass.
    image: /corporate/pictures/adriaan.jpg
    elected: 2022
---

KDE e.V. elects a board of directors, which represents KDE e.V.
and runs its business.

You can contact the board of KDE e.V. at <a
href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.

## Current Board

{{< people-list name="board" >}}

## Previous Boards

{{< previus-board >}}

Group photo of current and former board members at Akademy 2024 (from left to right: Nate Graham, Neofytos Kolokotronis, Eike Hein, Aleix Pol i Gonzàlez, Albert Astals Cid, Lydia Pintscher, Adriaan de Groot, Cornelius Schumacher, Eva Brucherseifer):
<img src="/corporate/pictures/boardAkademy2024.jpg" />
