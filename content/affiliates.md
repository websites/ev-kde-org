---
title: "KDE e.V. Affiliates"
menu:
  main:
    parent: organization
    weight: 7
    name: Affiliates
---

## Partners

The KDE e.V. partners with organizations around the world to advance its mission to support KDE.

+ KDE e.V. is an [Advisory Board member of the Document Foundation](https://www.documentfoundation.org/).
+ KDE e.V. is an [Associate Organization of the FSF Europe](https://fsfe.org/about/associates/associates.en.html).
+ KDE e.V. is a [member of the OASIS society](https://www.oasis-open.org/).
+ KDE e.V. is a [member of the Open Invention Network (OIN) community of licensees](https://openinventionnetwork.com/community-alphabetical/).
+ KDE e.V. is an [affiliate of the Open Source Initiative (OSI)](https://opensource.org/affiliates/list).
+ KDE e.V. is a [friend of OpenUK](https://openuk.uk/).

## Local organizations

The KDE e.V. has set up a local organization program to handle KDE activities and presence:

+ [KDE España](https://kde-espana.org)

## Community partners

+ [Here](/community-partners/) you can find the communities that partner with KDE e.V. through the <a href="/activities/partnershipprogram/">partnership program</a>.
