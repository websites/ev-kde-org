---
title: "Advisory Board"
menu:
  main:
    parent: organization
    weight: 6
---

The KDE e.V. Advisory Board provides a space for organizations to support KDE e.V. by giving feedback on KDE's activities and decisions. Furthermore, it is a channel to facilitate communication between KDE and the organisations on the Advisory Board.

## What can KDE e.V. Advisory Board offer to its members?

+ Regular meetings to give input and discuss topics of interest and relevance. In regular calls, we will discuss KDE initiatives which could benefit from KDE e.V. Advisory Board's input and identify opportunities for collaboration.
+ Access to maintainers and other key decision makers inside KDE. KDE e.V. Advisory Board members will hear about and give input on the latest technological innovations as well as social/policy endeavours inside KDE from the people who drive them.
+ Dedicated contact person. They are there to answer questions and put the KDE e.V. Advisory Board members in contact with the right people inside KDE.
+ Regular updates on important things going on inside KDE. We will provide the KDE e.V. Advisory Board with an overview of most important things inside KDE so that it can stay up to date.
+ Meetings at Akademy. We will have special KDE e.V. Advisory Board meetings for networking and direct exchange of ideas on the future of KDE.
+ Attendance at sprints. We are always happy to have KDE e.V. Advisory Board members joining our sprints in order to foster further collaboration with KDE contributors.

## What do we expect the KDE e.V. Advisory Board members to do?

+ Provide insights into the needs of various stakeholders.
+ Ability to combine our efforts to have more impact.
+ Get a more diverse view from outside of our organization on topics that are relevant to KDE.
+ Benefit from other organizations' experience.
+ Networking with other people and organizations.

## Joining the Advisory Board

Organizations can join the Advisory board either by becoming a <a href="/supporting-members/">KDE e.V. Patron</a> or by an explicit invitation by the <a href="/corporate/board/">KDE e.V. Board</a>.

## Current members

+ Blue Systems
+ Canonical
+ City of Munich
+ Debian
+ Document Foundation
+ FOSS Nigeria
+ FSF
+ FSFE
+ g10 Code
+ Kubuntu Focus
+ MBition
+ OpenUK
+ OSI
+ Qt Group
+ Slimbook
+ SUSE
+ TUXEDO Computers
