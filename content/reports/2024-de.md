# Mitgliederversammlung KDE e.V. 2024

Die Mitgliederversammlung findet am Samstag, den 19. Oktober 2024 um 15:00 MESZ online über die passwortgeschützten Open-Source-Videokonferenzsoftware BigBlueButton statt.
Tagesordnung und Präsentationen werden über das Open-Source-Tool OpenSlides verwaltet und bereitgestellt. In OpenSlides wird auch die Anwesenheit der Mitglieder registriert, die Vertreter-Stimmen verwaltet sowie die anstehenden Wahlen durchgeführt. Eine sichere, geheime, und überprüfbare Wahl ist durch dieses System gewährleistet. Beide Systeme werden auf KDE-Servern betrieben. Alle Mitglieder haben im Vorfeld der Versammlung Zugriff auf die beiden Systeme erhalten. Die Liste der Teilnehmer wird zur Dokumentation aus OpenSlides exportiert.

Zu Beginn der Veranstaltung sind  79 Mitglieder anwesend.
11 nicht anwesende Mitglieder haben einen Vertreter benannt.

## Tagesordnung

1. Begrüßung
2. Wahl des Versammlungsleiters
3. Bericht des Vorstands
   1. Bericht über Aktivitäten
   2. Bericht des Schatzmeisters
   3. Bericht der Rechnungsprüfer
   4. Entlastung des Vorstandes
4. Bericht der Vertreter und Arbeitsgruppen des Vereins
   1. Bericht der Vertreter in der KDE Free Qt Foundation
   2. Bericht der KDE Free Qt Working Group
   3. Bericht der System Administration Working Group
   4. Bericht der Community Working Group
   5. Bericht der Financial Working Group
   6. Bericht der Advisory Board Working Group
   7. Bericht der Fundraising Working Group
5. Wahl der Rechnungsprüfer
6. Wahl der Vertreter in der KDE Free Qt Foundation
7. Verschiedenes

## Protokoll

Alle Berichte wurden den Mitgliedern im Vorfeld per Email zugesandt und am 07.09.2024 zusätzlich auf der durch den Verein organisierten Konferenz [Akademy](https://akademy.kde.org/2024) öffentlich vorgestellt.

### Begrüßung und Wahl des Versammlungsleiters

Um 15:05 eröffnet der Vorsitzende des Vorstands, Aleix Pol i Gonzàlez, die Versammlung
und schlägt Harald Sitter als Versammlungsleiter vor.

Es gibt keine Gegenstimmen und  somit ist Harald Sitter zum Versammlungsleiter gewählt.

Der Versammlungsleiter benennt David Redondo als Protokollant und stellt fest, dass das satzungsgemäße Quorum erfüllt ist.

Der Versammlungsleiter stellt fest, dass die Einladung zur Mitgliederversammlung ordnungsgemäß und fristgerecht - per Emailversand am  05.09.2024  mit vorläufiger und erneut am 05.10.2024 mit finaler Tagesordnung erfolgt ist.

Es gibt keine Einsprüche gegen die Tagesordnung und es wurden keine weiteren Punkte angebracht.

### Bericht des Vorstands

#### Bericht über Aktivitäten

Der aktuelle Vorstand besteht aus Aleix Pol i Gonzàlez (Vorsitzender), Eike Hein (Schatzmeister und Stellvertreter des Vorsitzenden), Lydia Pintscher (Stellvertreterin des Vorsitzenden), Adriaan de Groot und Nathaniel Graham.

10 neue aktive Mitglieder wurden in den Verein seit der letzten Mitgliederversammlung aufgenommen.

Der Verein hat durch die Plasma 6 Spendenaktion sehr viele neue Fördermitglieder gewonnen. Insgesamt gibt es nun 719 Fördermitglieder im Vergleich zu 53 letztes Jahr.
Zusätzlich kann der Verein auch einen Zuwachs an Unterstützern auf Github von 67 letztes Jahr auf nun 132 und wiederkehrenden Spendern von 130 auf 179 auf der Platform Donorbox verzeichnen.
Bei den Patronen und Unterstützern von KDE gab es keinen Änderung.

Der Verein unterhält vielfältige Partnerschaften mit anderen Organisationen und ist Mitglied in verschiedenen Organisationen im Bereich Open Source.

Der Verein hat drei Angestellte und neun freie Mitarbeiter. Erstere  sind Petra Gillert  als Assistenz des Vorstands und Projekt- und Communitymanager Joseph P. De Veaugh-Geiss, sowie nun auch Nicole Teal, die beide nun das Projekt "Nachhaltige Software Für Nachhaltige Hardware" betreuen.
Weiterhin für den Verein tätig sind, Dina Nouskali als Event-Koordinatorin, Aniqa Khokhar und Paul Brown  als Marketing-Berater, Thiago Masato Costa Sueto für Dokumentation und Nicolas Fella als Software Platform Engineer. Im Bereich  Hardware Integration ist Natalie Clarius weiter tätig und seit kurzem nun auch Jakob Petsovits.
Farid Abdelnour übernimmt die Aufgaben des Goals-Projekt-Koordinator von Adam Szopa.
Das Projekt von Ingo Klöcker als  App Stores Engineer wurde erfolgreich abgeschlossen.
Kuntal Bar war für drei Monate für Arbeit an LabPlot beauftragt.

Der Vorstand dankt den Organisatoren der diesjährigen Akademy Konferenz. Für nächstes Jahr wird noch ein Ausrichtungsort gesucht.
Im letzten Jahr gab es drei Sprints. Der Vorstand ermutigt weiter mehr Sprints zu organisieren.
Es wurden viele  Konferenzen und Messen besucht und der Vorstand ist zufrieden mit der Anzahl und Auswahl der Veranstaltungen.
Von KDE organisiert wurden dieses Jahr Akademy, Akademy-es, Linux App Summit und zur Freude des Vorstands nun auch wieder conf.kde.in.

CiviCRM wurde nun vollständig durch Donorbox ersetzt.
Das Nachfolgeprojekt von BE4FOSS ist das bereits erwähnte Projekt "Nachhaltige Software Für Nachhaltige Hardware" und wird mit Mitteln des Umweltbundesamtes gefördert. Eine erste Anwendung, die gegen Bezahlung erhältlich ist, steht nun in einem Appstore zur Verfügung.
Weiterhin ist KDEnlive das einzige Projekt, für das direkt Unterstützung eingeworben wird. Der Vorstand ist offen weitere Projekte in das Programm aufzunehmen.

Die KDE Gemeinschaft hat sich neue Ziele gegeben, die unterstützt werden sollen. Der Prozess wurde angepasst und ist nun teamorientierter und enthält mehr Öffentlichkeitsarbeit.

Erreichte Ziele letztes Jahres beinhalten die Unterstützung des KDE Goals Prozesses, Durchführung der Konferenzen Akademy und LAS und die Anpassung an die geänderte finanzielle Situation - jedoch konnte das Patron von KDE Programm noch nicht überarbeitet werden. Es wurde Erfahrung gesammelt als Arbeitgeber für mehrere Personen. Die Arbeit mit dem  Ziel den Abstand zu den Nutzer zu verringern ist noch nicht abgeschlossen
und auch konnten die Spenden, die für KDEnlive eingesammelt wurden noch nicht ausgegeben werden.

Als Schlüsselpunkte für die Zukunft sieht der Vorstand die Unterstützung der Ziele die sich die Gemeinschaft gegeben hat,
die Weiterentwicklung des Patron von KDE Programms, nachhaltige Nutzung der Spendenzugewinne, sowie Ausnutzen der Möglichkeiten für die Gemeinschaft sich zu treffen wie Akademy und Sprints.
Weitere Punkte sind die Schaffung von Mechanismen um Mitarbeiter weiter beschäftigen zu können, Verbesserung der Zusammenarbeit mit Hardware Partnern und Verbreitung von KDE Anwendungen in App Stores um auch so Einnahmen zu generieren.

Der Vorstand beantwortet Fragen zu den vorgestellten Aktivitäten.

#### Bericht des Schatzmeisters

Eike Hein erläutert kurz den Finanzbericht, den er mit Hilfe der Financial Working Group und des Vorstands erstellt hat und verweist
auf Stellen, an denen weitere detaillierte Informationen gefunden werden können.

##### Finanzjahr 2023

Im Finanzjahr 2023 verhielten sich die Einnahmen und Ausgaben wie geplant. Einnahmen und Ausgaben stiegen an und es wurde wie im vergangenen Jahr ein geplantes Defizit erzielt.
Eine Sache, die in der Zukunft beachtet werden sollte, ist die Abnahme von Sponsormitteln für Konferenzen.


##### Finanzplan 2024


Ziel für 2024 ist es durch Steigerung der Einnahmen und Förderung das geplante Defizit auf 20% zu verringern. Gleichzeitig sollen die Ausgaben nicht weiter steigen.
2026 oder 2027 soll der Haushalt ausgeglichen sein.

#### Bericht der Kassenprüfer
Der Bericht der Kassenprüfer wurde vor der Versammlung den Mitglieder bereits am 15.10.24 per Email zugesandt.

Die Kassenprüfer Thomas Baumgart und Andreas Cord-Landwehr haben am 12.10.2024 die Prüfung bei einem Treffen im Büro des Vereins mit Lydia Pintscher, Eike Hein und Petra Gillert durchgeführt und haben Einsicht in die Bücher genommen.
Thomas stellt  den Bericht der Versammlung vor.
Die Rechnungsprüfer zeigen sich zufrieden dass Verbesserungsvorschläge umgesetzt wurden.
Es wird eine ordnungsgemäße Buchführung attestiert. Alle Fragen der Kassenprüfer wurden zu deren Zufriedenheit beantwortet.

Die Kassenprüfer empfehlen die Entlastung des Vorstands für das Finanzjahr 2023.

#### Entlastung des Vorstands

Der Versammlungsleiter bittet um Meldung, ob Einwände gegen die Entlastung bestehen. Es gibt keine Meldungen.

Die Abstimmung wird jetzt durchgeführt. Die Mitglieder des Vorstandes nehmen an dieser Abstimmung nicht teil. Das Abstimmungsergebnis lautet:

* Für die Entlastung:     **83**
* Gegen die Entlastung:   **0**
* Enthaltungen:           **2**
* Abstimmungsteilnehmer:  **85**

Mit diesem Ergebnis ist der Vorstand für die vergangene Berichtsperiode entlastet.

### Bericht der Vertreter und Arbeitsgruppen des Vereins

Hier werden von den einzelnen Gruppen lediglich Ergänzungen gegeben, die nicht für die Öffentlichkeit bestimmt sind und daher nicht in den vorab versandten Berichten und der öffentlichen Präsentation enthalten sind.

   1. Bericht der Vertreter in der KDE Free Qt Foundation
   2. Bericht der KDE Free Qt Working Group
   3. Bericht der System Administration Working Group
   4. Bericht der Community Working Group
   5. Bericht der Financial Working Group
   6. Bericht der Advisory Board Working Group
   7. Bericht der Fundraising Working Group

#### Bericht der Bericht der Vertreter in der KDE Free Qt Foundation

Albert Astals Cid berichtet von den Aktivitäten der KDE Free Qt Foundation. Es gab kein Stiftungstreffen dieses Jahr, jedoch wurden
während Akademy Gespräche geführt. Die Beziehungen zu der Qt Company schätzt er zurzeit als gut ein. Die Stiftung hatte durch administrative
Fehler den Zugang zu ihren Bankkonten verloren, damit dies nicht wieder geschieht, möchte Albert direkt Zugriff auf die Bankgeschäfte bekommen, muss sich
jedoch erst in Norwegen registrieren.

#### Andere Gruppen

Die anderen Arbeitsgruppen haben über die in den öffentlich zugänglichen Informationen keine weiteren Punkte zu berichten.

### Wahl der Rechnungsprüfer

Die beide bisherigen Rechnungsprüfer Andreas Cord-Landwehr und Thomas Baumgart stellen sich für eine Wiederwahl. Es gibt keine weiteren Kandidaten.

Zwei Mitglieder sind inzwischen der Versammlung beigetreten.

Die abgegebenen Stimmen ergeben folgendes Wahlergebnis für die Kandidaten:

* Andreas Cord-Landwehr:   **90**
* Thomas Baumgart:         **90**
* Wahlteilnehmer:          **92**

Thomas nimmt nach Rückfrage des Versammlungsleiters die Wahl an. Andreas ist nicht anwesend hat jedoch vor der Versammlung mit seiner Kandidatur bekannt gegeben, dass
er im Falle einer Wahl diese auch annehmen werde. Dies hat er auch nach der Versammlung getan.

### Wahl der Vertreter in der KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer und Albert Astals Cid stehen für eine Wiederwahl zur Verfügung. Es gibt keine weiteren Kandidaten.

Die abgegebenen Stimmen ergeben folgendes Wahlergebnis für die Kandidaten:

* Olaf Schmidt-Wischhöfer: **90**
* Albert Astals Cid:       **88**
* Wahlteilnehmer:          **92**

Beide Kandidaten nehmen die Wahl nach Rückfrage durch den Versammlungsleiter an.

### Verschiedenes

Es wird daraufhin hingewiesen, dass die Arbeitsgruppen mehr Mitglieder benötigen.

### Schluss der Versammlung
Harald Sitter beendet die Versammlung um 16:45.
<br />
<br />
<br />
<br />
<br />
<br />
David Redondo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Harald Sitter<br/>
(Protokollant)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(Versammlungsleiter)<br/>
