---
title: "Annual General Meeting KDE e.V. 2022"
layout: page
---

The general assembly took place on Monday, October 3rd 2022 at 10:00 (CEST) in Edifici Vèrtex, Universitat Politècnica de Catalunya, Plaça Eusebi Güell 6, 08034 Barcelona, Spain.

At the beginning of the meeting 44 members of the association are present. 37 members who are not present have named a proxy for their vote.  Joseph P. De Veaugh-Geiss, a *freier Mitarbeiter* of the association for the Blaue Engel Project, joins the meeting as a guest.

## Agenda

1. Welcome
2. Election of the meeting chairperson
3. Reports from the board
   1. Report on activities
   2. Report from the treasurer
   3. Report from the auditors of accounting
   4. Relief of the board
4. Report from representatives and working groups
   1. Report from the representatives to the KDE Free Qt Foundation
   2. Report from the KDE Free Qt Working Group
   3. Report from the System Administration Working Group
   4. Report from the Community Working Group
   5. Report from the Financial Working Group
   6. Report from the Advisory Board Working Group
   7. Report from the Fundraising Working Group
5. Election of the board
6. Election of the auditors of accounting
7. Election of the representatives to the KDE Free Qt Foundation
8. Miscellaneous

## Minutes

All reports were sent to the members in advance via email and were additionally presented publicly on October 2nd 2022 as part of the [Akademy](https://akademy.kde.org/2022) conference which is organized by the e.V.

### Welcome and election of the meeting chairperson

At 10:32 president Aleix Pol i Gonzàlez opens the assembly.

Harald Sitter is elected as meeting chairperson by show of hands.

Joseph P. De Veaugh-Geiss is allowed attendance of the assembly as guest with one abstention.

The meeting chairperson notes that the invitations to the general assembly were sent out properly and in a timely manner - by email on August 21st 2022 containing the preliminary agenda, and again on September 19th 2022 containing the final agenda (with no changes to the preliminary one).

The agenda is confirmed by show of hands and no additional items were raised.

The meeting chairperson appoints David Redondo as record keeper of the minutes and notes that the statutory quorum for the AGM is reached.

### Report of the board

#### Report on activities

The current board consists of Aleix Pol i Gonzàlez (chairperson of the board) Eike Hein (treasurer and deputy chairperson of the board), Lydia Pintscher (deputy chairperson of the board), Adriaan de Groot, and Neofytos Kolokotronis.
This assembly marks the end of the terms of office for Adriaan de Groot and Neofytos Kolokotronis.

8 new active members have joined the e.V. since the last general assembly, and one active member resigned.

The e.V.again lost some supporting members. Compared to 71 last year there are only 48 this year.
Via Github, a new way of donating to the e.V. was opened. This option is already being used by 45 persons and organizations.

A new Patron was gained: Tuxedo Computers. Tuxedo is also a member of the Advisory Board. Kontent GmbH dropped their support.

The e.V. has one employee and seven freelancers. Petra Gillert is employed as assistant to the board, Aniqa Khokhar and Paul Brown work as marketing consultants, and Adam Szopa as project coordinator. New freelancers are Dina Nouskali as coordinator of events, Joseph P. De Veaugh-Geiss and Lana Lutz as representatives for the Blauer Engel project, and Ingo Klöcker as App Store Engineer.

Allyson Alexandrou quit her assignment in December 2021, Frederik Schwarzer in September 2021 and Carl Schwan in June 2021. The board is trying to fill the open positions for Software Development, Documentation and Hardware Integration.

Last year a few in-person sprints were held alongside online sprints. The board encourages the members to organize more sprints. A few conferences were staffed by KDE e.V.
Also last year KDE celebrated its 25th anniversary. A few activities were organized for this event.
The location for Akademy 2023 has been announced. Sprints and meetings of the members shall be supported.

In connection with the Kdenlive project, the e.V. has started a pilot project to collect donations for specific projects.

The KDE community has agreed on new goals that shall be supported.

A member asks if all collected donations are to be allocated to the Kdenlive project - the e.V. will keep a certain percentage to cover the administrative efforts.

#### Report from the treasurer

Eike Hein explains the financial report that he created with the help of the financial working group and the board.

##### Fiscal year 2021

The financial situation is similar to 2020 and the e.V. is financially stable. There was an increase in both revenues and spending.
Once again the target of spending more money was missed - because of the pandemic there were very few travels, and it was harder than expected to fill some of the advertised job openings.
At the beginning of 2021 there was a major donation by PINE64, and sponsoring payments for Linux App Summit 2020 were received.

Eike then proceeds to illustrate some details of revenues and spending using graphical analysis.

##### Fiscal plan 2022

On the revenue side a moderate increase in revenue is expected. Although revenue from Google Summer of Code is decreasing, the Blauer Engel project has brought in a new grant revenue. The liquidation of the KDE League Inc. is advancing. Its capital will fall to the e.V.

A strong increase in spending is expected because more travel will take place, and because more people are and will be employed.

Afterwards questions from the membership regarding the reports are answered.

#### Report from the auditors of accounting

The report from the auditors of accounting was sent before the general assembly to the membership on July 30th 2022 via email.

The auditors of accounting have met Lydia Pintscher and Petra Gillert on July 23rd 2022 in the e.V. office and have examined the books.
Proper accounting is attested. All questions of the auditors of accounting were answered to their satisfaction.

The auditors of accounting recommend that the board be relieved for the fiscal year 2021.

#### Relief of the board

The chairperson of the meeting asks whether there are any objections against the relief. No objections are raised.

The vote is done. The members of the board do not partake in this vote. The voting results are as follows:

* In favor of the relief: **75**
* Against the relief:     **0**
* Abstentions:            **0**
* Participants:           **75**

By this voting results the board is relieved.

### Report of the representatives and working groups of the e.V.

At this point the single groups simply add slight additions which are not meant for the public and are therefore not part of the pre-published reports and public presentations.

#### Report from the respresentatives to the KDE Free Qt Foundations / the KDE Free Qt Working group

Albert Astals Cid reports as representative of the KDE e.V. from the KDE Free Qt Foundation.

Albert is now chairperson of the foundation. Lars Knoll, a representative by the Qt Company, has left the foundation. A new representative has not yet been named.

Albert discusses some activities in detail and answers related questions.

#### Community Working Group

The community working group answers questions from the membership regarding common practice and specific incidents.

#### Other groups

The other groups have nothing to add to the publicly available information.

### Election of the board

The term of office of the board members Adriaan de Groot and Neofytos Kolokotronis come to their end with this assembly. Neofytos and Adriaan are up for reelection. Additional candidates are Andy Betts and Nathaniel Graham. There are no further candidates.

The candidates briefly introduce themselves and answer questions from the members.

A secret vote is held. Each member has two votes. The cast votes yield the following results:

* Adriaan de Groot:        **56**
* Neofytos Kolokotronis:   **43**
* Andy Betts:              **14**
* Nathaniel Graham:        **48**
* Abstentions              **1**
* Votes:                   **162**

Adriaan and Nathaniel accept the results.

### Election of the auditors of accounting

Of the two current auditors of accounting, Andreas Cord-Landwehr is up for reelection. Ingo Klöcker is not up for reelection. Additionally, Thomas Baumgart runs for office. There are no further candidates.

The members vote for both candidates in a combined vote. The cast votes yield the following results:

* In favor:        **78**
* Abstentions:     **3**
* Participants:    **81**

Andreas accepts the vote when prompted by the chairperson of the meeting. Thomas has declared in advance in writing that he will accept the vote should he be elected.

### Election of the representatives to the KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer and Albert Astals Cid are available for reelection. There are no further candidates.

The members vote for both candidates in a combined vote. The cast votes yield the following results:

* In favor                 **76**
* Abstentions:             **5**
* Participants:            **81**

Both candidates accept the vote when prompted by the chairperson of the meeting.

### Miscellaneous
 
No additional points are discussed.

### Closing of the assembly
Harald Sitter closes the assembly at 12:46.
<br />
<br />
<br />
<br />
<br />
<br />
David Redondo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Harald Sitter<br/>
(Keeper of the protocol)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(Chairperson of the meeting)<br/>
October 4th 2022
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
October 4th 2022
