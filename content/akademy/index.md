---
title: Akademy
menu:
  main:
    parent: activities
    weight: 1
---

Since 2003 the KDE e.V. has organized an annual meeting of the KDE community. It
includes a conference, time for coding, meetings and
the Annual General Meeting (AGM, or the annual membership assembly) of the e.V.
It has been known under the name 
[Akademy](https://akademy.kde.org) since 2004. It is the biggest meeting
of the worldwide KDE community.

## Akademy History

- [Akademy 2024, Würzburg](https://akademy.kde.org/2024)
- [Akademy 2023, Thessaloniki](https://akademy.kde.org/2023)
- [Akademy 2022, Barcelona](https://akademy.kde.org/2022)
- [Akademy 2021, Online](https://akademy.kde.org/2021)
- [Akademy 2020, Online](https://akademy.kde.org/2020)
- [Akademy 2019, Milan, Italy](https://akademy.kde.org/2019)
- [Akademy 2018, Vienna, Austria](https://akademy.kde.org/2018)
- [Akademy 2017, Almería, Spain](https://akademy.kde.org/2017)
- [Akademy 2016 (as part of QtCon), Berlin, Germany](https://akademy.kde.org/2016)
- [Akademy 2015, A Coruña, Galicia, Spain](https://akademy.kde.org/2015)
- [Akademy 2014, Brno, Czech Republic](https://akademy.kde.org/2014)
- [Akademy 2013, Bilbao, Basque Country, Spain](https://akademy.kde.org/2013)
- [Akademy 2012, Tallinn, Estonia](https://akademy2012.kde.org)
- [Akademy 2011 (Desktop Summit), Berlin, Germany](https://www.desktopsummit.org)
- [Akademy 2010, Tampere, Finland](https://akademy2010.kde.org)
- [Akademy 2009 (Desktop Summit), Las Palmas, Gran Canaria](https://akademy2009.kde.org)
- [aKademy 2008, Sint-Katelijne-Waver, Belgium](https://akademy2008.kde.org)
- [aKademy 2007, Glasgow, Scotland](https://akademy2007.kde.org)
- [aKademy 2006, Dublin, Ireland](https://akademy2006.kde.org)
- [aKademy 2005, Malaga, Spain](https://conference2005.kde.org)
- [aKademy 2004, Ludwigsburg, Germany](https://conference2004.kde.org)
