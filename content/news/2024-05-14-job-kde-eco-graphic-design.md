---
title: 'KDE e.V. is looking for a graphic designer for environmental sustainability project'
date: 2024-05-14 12:30:00
layout: post
noquote: true
---

Edit 2024-05-21: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a graphic designer to implement materials (print design, logo design, infographics, etc.) for a new environmental sustainability campaign within KDE Eco. Please see the [job ad](/resources/jobad-KDE-Eco-Graphic-Designer_2024.pdf) for more details about this employment opportunity.

We are looking forward to your application.
