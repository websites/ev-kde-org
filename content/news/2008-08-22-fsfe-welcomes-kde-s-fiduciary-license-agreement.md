---
title: "FSFE Welcomes KDE's Fiduciary License Agreement"
date: 2008-08-22 00:00:00 
layout: post
---

Free Software Foundation Europe welcomes the adoption of the Fiduciary
Licence Agreement by the K Desktop Environment project. The FLA is a copyright
assignment that allows Free Software projects to assign their copyright to
single organisation or person. This enables projects to ensure their legal
maintainability, including important issues such as preserving the ability
to re-license and certainty to have sufficient rights to enforce licences
in court.

<em>"We see the adoption of the FLA by KDE as a positive and important milestone 
in the maturity of the Free Software community," says Georg Greve, president 
of Free Software Foundation Europe. "The FLA was designed to help projects 
increase the legal maintainability of their software to ensure long-term 
protection and reliability. KDE is among the most important Free Software 
initiatives and it is playing a central role in bringing freedom to the desktop. 
This decision of the KDE project underlines its dedication to think about how 
to make that freedom last."</em>
<p />

Adriaan de Groot, Vice President of KDE e.V., the organisation behind
the KDE project, said <em>"KDE e.V. has endorsed the use of a particular FLA 
based directly on the FSFE's sample FLA as the preferred way to assign 
copyright to the association.  We recognise that assignment is an option 
that individuals may wish to exercise; it is in no way pushed upon KDE 
contributors. There are also other avenues of copyright assignment available 
besides the FLA, but we believe this is the easiest way to get it done, with 
little fuss. Enthusiasm for the FLA was immediate -- people were asking for 
printed versions of the form before the week was out so that they could fill 
one in."</em>
<p />

"The FLA is a versatile document designed to work across different
countries with different perceptions of copyright and authorship,"
says Shane Coughlan, Freedom Task Force coordinator.  "As a truly
international project, KDE provides a great example of how the FLA can
provide legal coherency in the mid-to-long term.  It's been a pleasure
to help with the adoption process and FSFE's Freedom Task Force is ready 
to continuing supporting KDE in the future."
<p />

KDE's adoption of the FLA is the result of cooperation between 
KDE e.V. and FSFE's Freedom Task Force over the last year and a half, 
part of the deepening collaboration between the two associate 
organisations.
<p />

<h3>About the FLA:</h3>
<p>
The FLA was written by Dr. Axel Metzger (ifross) and Georg Greve (FSFE) in 
consultation with renowned international legal and technical experts. Parties 
involved in the evolution of the FLA at some point or another included RA Dr. 
Till Jaeger, Carsten Schulz, Prof. Eben Moglen, RA Thorsten Feldmann, LL.M., 
Werner Koch, Alessandro Rubini, Reinhard Muller and others. The latest 
revision was compiled by Georg Greve and FSFE's FTF coordinator Shane M 
Coughlan based on feedback provided by Dr. Lucie Guibault of the Institute 
for Information Law in the Netherlands.
</p>

<h3>About KDE:</h3>
<p>
KDE is an international technology team that creates free and open source 
software for desktop and portable computing. Among KDE's products are a modern 
desktop system for Linux and UNIX platforms, comprehensive office productivity 
and groupware suites and hundreds of software titles in many categories 
including Internet and web applications, multimedia, entertainment, 
educational, graphics and software development. KDE software is translated 
into more than 60 languages and is built with ease of use and modern 
accessibility principles in mind. KDE4's full-featured applications run 
natively on Linux, BSD, Solaris, Windows and Mac OS X. 
</p>

<h3>About the Free Software Foundation Europe:</h3>

   The Free Software Foundation Europe (FSFE) is a non-profit
   non-governmental organisation active in many European countries and
   involved in many global activities. Access to software determines
   participation in a digital society. To secure equal participation in the
   information age, as well as freedom of competition, the Free Software
   Foundation Europe (FSFE) pursues and is dedicated to the furthering of
   Free Software, defined by the freedoms to use, study, modify and copy.
   Founded in 2001, creating awareness for these issues, securing Free
   Software politically and legally, and giving people Freedom by
   supporting development of Free Software are central issues of the FSFE.
<p />
The Freedom Task Force can be found at <a href="http://www.fsfeurope.org/ftf/">http://www.fsfeurope.org/ftf/</a><br />
The Freedom Task Force can be emailed at ftf at fsfeurope.org

<h3>Contact:</h3>

   You can reach the FSFE switchboard from:<br /><br />
    <br />Belgium:     +32 2 747 03 57
    <br />Germany:     +49 700 373 38 76 73
    <br />Sweden:      +46 31 7802160 
    <br />Switzerland: +41 43 500 03 66 
    <br />UK:          +44 29 200 08 17 7 
<p />

   Shane Coughlan, FTF Coordinator,  FSFE extension: 408
<p />
   Further information: http://fsfeurope.org

      
