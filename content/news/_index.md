---
title: News
subtitle: Keep up with the latest news about the KDE e.V.
menu:
  main:
    parent: info
    weight: 2
layout: news
---

