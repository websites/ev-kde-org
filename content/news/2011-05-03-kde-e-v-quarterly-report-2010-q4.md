---
title: 'KDE e.V. Quarterly Report 2010 Q4'
date: 2011-05-03 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is proud to publish the <a href="http://ev.kde.org/reports/ev-quarterly-2010Q4.pdf">last quarterly report for 2010</a>.

This report covers October through December, 2010, including statements from the board and the working groups of KDE e.V. about their activities, reports from sprints, financial information and more.
      
