---
title: 'KDE e.V. Quarterly Report 2012 Q4'
date: 2013-03-08 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2012_Q4.pdf">fourth quarterly report of 2012</a>.

This report covers October through December 2012, including statements from the board, reports from sprints, financial information and a feature about KDE Espa&#241;ia.
