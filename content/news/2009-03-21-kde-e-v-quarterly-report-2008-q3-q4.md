---
title: 'KDE e.V. Quarterly Report 2008 Q3/Q4'
date: 2009-03-21 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2008Q3-Q4.pdf">KDE e.V.
Quarterly Report</a> is now available for July to December 2008. This document includes
reports of the board and the working groups about the KDE e.V. activities of the last two
quarters of 2008, and future plans.
