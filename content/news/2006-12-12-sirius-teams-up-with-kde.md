---
title: 'Sirius teams up with KDE'
date: 2006-12-12 00:00:00 
layout: post
---

In a move to promote the KDE desktop in the Enterprise, the UK's Open Source
experts, <a href="http://www.siriusit.co.uk/">Sirius Corporation</a>,
have become a <a href="/supporting-members">Supporting
Member</a> of the KDE project.  Sirius' commitment to KDE is our second
supporting membership and follows Canonical's recent patronage of the project.
<a href="http://www.siriusit.co.uk/">Sirius</a> and KDE are joint participants
in <a href="http://www.sqo-oss.eu/">SQO-OSS</a>, an EU-funded project
that assesses the quality of Open Source code.

Mark Taylor, CEO, Sirius Corporation said: <em>"KDE is a high-quality
desktop that has always had a strong following in the Open Source
community. By becoming a sponsor of the project, we now want to raise
its profile in the Enterprise."</em>

Sebastian K&#252;gler of the <a href="/">KDE e.V.</a> Board
said: <em>"KDE is happy to welcome
Sirius as one of Europe's leading Open Source experts in the circuit of
KDE's Supporting Members. For KDE, this agreement manifests the next
natural step in the improvement of relationships between KDE as the
leading Free Desktop and leaders in the Open Source industry."</em>

KDE e.V. wishes to thank its current supporters, and would like to
invite all interested parties to <a href="/getinvolved/supporting-members/">
 help us continue to serve the KDE community</a>.
