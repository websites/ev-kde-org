---
title: 'KDAB becomes Patron of KDE'
date: 2007-07-03 00:00:00 
layout: post
---

The KDE e.V. and <a href="https://www.kdab.com/">KDAB</a> are happy to announce continued
collaboration on the Free Desktop by <a href="https://www.kdab.com/">KDAB</a> being the latest new
<a href="/supporting-members/">Patron of KDE</a>. <a href="https://www.kdab.com/">KDAB</a>
is known for its high-quality software
services. <a href="https://www.kdab.com/">KDAB</a> explains <em>"KDE and <a href="https://www.kdab.com/">KDAB</a> have a very strong relationship that dates back to the
beginning of the KDE project, with the <a href="https://www.kdab.com/">KDAB</a> CEO, Kalle Dalheimer,
being one of the founders of the KDE project. Since then, <a href="https://www.kdab.com/">KDAB</a>
has supported KDE consistently both with code and funding, especially for the past aKademies. We
believe that a strong KDE community is good for all of us, and thus want to provide KDE e.V. with a
stable and plannable stream of funding."</em>

<div style="border: 0px; float: right;">
    <img src="http://enterprise.kde.org/logos/kdab.png" />
</div>

Sebastian K&#252;gler of the KDE e.V. Board of Directors adds: <em>"<a href="https://www.kdab.com/">KDAB</a> is one of the companies that understand their role in a Free
Software community very well. <a href="https://www.kdab.com/">KDAB</a> is actively involved with
making the KDE software the success it is and will be. Additionally, <a href="https://www.kdab.com/">KDAB</a> takes its contribution to KDE very seriously. This is shown by
their thorough commitment to a stable, reliable and well-maintained codebase, and is stressed by
their involvement in KDE as an organisation. <a href="https://www.kdab.com/">KDAB</a> serves as an
example for excellent collaboration with other parts of the community. We're thrilled that we're
able to manifest our collaboration also in this more formal way."</em>

<div style="border: thin solid grey; margin: 1ex; padding: 1ex; float: left;">
    <img height="24" src="http://static.kdenews.org/jr/patron_small.png" width="91" />
</div>

Mirko Boehm, KDE core developer and head of <a href="http://www.kdab.com/">KDAB</a>'s Berlin office
- and thus host of the recent KDEPIM and KOffice meetings - explains <a href="https://www.kdab.com/">KDAB</a>'s
involvement in the KDE community as follows: <em>"<a href="https://www.kdab.com/">KDAB</a> employs numerous KDE developers, who work directly or
indirectly on KDE in both their spare time and their work hours. In fact, we are a company mostly of
KDE developers. 3 of the top ten committers to KDE are <a href="https://www.kdab.com/">KDAB</a>ians,
according to <a href="http://www.ohloh.net">ohloh.net</a>, including David Faure, one of the major
contributors to KDE's core. We are actively trying to increase the visibility of KDE through our
customer contacts and look forward to promote the powerful KDE 4 platform in future projects."</em>

