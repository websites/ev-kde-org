---
title: 'KDE e.V. Quarterly Report 2011 Q4'
date: 2012-03-20 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2011_Q4.pdf">fourth quarterly report of 2011</a>.

This report covers October through December 2011, including statements from the board, reports from sprints, financial information and a special part about KDE's 15th birthday.
      
