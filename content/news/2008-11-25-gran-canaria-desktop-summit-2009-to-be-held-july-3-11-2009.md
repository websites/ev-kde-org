---
title: 'Gran Canaria Desktop Summit 2009 to be held July 3-11, 2009'
date: 2008-11-25 00:00:00 
layout: post
---

The inaugural Desktop Summit, uniting the flagship conferences of the
GNOME and KDE communities, GUADEC and Akademy, will be held in Gran
Canaria, Canary Islands, Spain the week of July 3-11, 2009. The conference
will be hosted by Cabildo, the local government of Gran Canaria.

The GNOME and KDE communities will use this co-located event to
intensify momentum and increase collaboration between the projects. It
gives a unique opportunity for key figures to collaborate and improve
the free and open source desktop for all.

Please visit the official
<a href="http://www.grancanariadesktopsummit.org">website</a> for further
information.
