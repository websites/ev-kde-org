---
title: 'KDE e.V. Membership Council'
date: 2005-09-01 00:00:00 
layout: post
---

The 2005 assembly meeting took place at <a href="http://conference2005.kde.org">aKademy
2005</a> in Malaga, Spain.  A new board was elected and the assembly
votes to add technical guidance as a responsibility of KDE e.V. and
create working groups for KDE development and activity.  <a href="/meetings/2005/">Notes from the assembly meeting</a> and the
<a href="meetings/2005-working-groups-discussion.php">working group
meeting</a> are available.
