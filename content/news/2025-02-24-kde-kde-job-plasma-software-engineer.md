---
title: 'KDE e.V. is looking for a Plasma software engineer'
date: 2025-02-24 10:00:00
layout: post
noquote: true
---

KDE e.V., the non-profit organization supporting the KDE community, is looking for a contractor to improve KDE’s Plasma desktop environment in ways that support user acquisition through growth into new hardware and software markets. The Plasma software engineer will address defects and missing features that are barriers to these objectives. Please see the [full job listing](/resources/jobad-plasma-software-engineer-2025.pdf) for more details about this opportunity.
We are looking forward to your application.
