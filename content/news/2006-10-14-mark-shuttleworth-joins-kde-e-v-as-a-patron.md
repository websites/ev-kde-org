---
title: 'Mark Shuttleworth joins KDE e.V. as a Patron'
date: 2006-10-14 00:00:00 
layout: post
---

At the event celebrating the 10th birthday of the KDE project,
Eva Brucherseifer, the president of the KDE e.V., announced that Mark
Shuttleworth, the man behind Canonical and Ubuntu, joined the KDE e.V.
under the supporting membership program as the first Patron.

