---
title: 'MBition becomes a KDE patron'
date: 2024-05-07 9:30:00
layout: post
noquote: true
slug: mbition-becomes-kde-patron
alias:
 - ./decisions
categories: [Patrons]
---

![Mbition header image with logo](mbition_header.jpg)

**MBition supports the work of the KDE community with its generous sponsorship.**

[MBition](https://mbition.io/) designs and implements the infotainment system
for future generations of Mercedes-Benz cars and utilizes KDE's technology and
know-how for its products.

"*After multiple years of collaboration across domains, we feel that becoming a patron of KDE e.V is the next step in deepening our partnership and furthering our open-source strategy*" says Marcus Mennemeier, Chief of Technology at MBition.

"*We are delighted to welcome MBition as a Patron,*" says Lydia Pintscher, Vice President of KDE e.V. "*MBition has been contributing to KDE software and the stack we build on it for some time now. This is a great step to bring us even closer together and support the KDE community, and further demonstrates the robustness and hardware readiness of KDE's software products.*"

MBition joins KDE e.V.'s other patrons: Blue Systems, Canonical, g10 Code, Google, Kubuntu Focus, Slimbook, SUSE, The Qt Company and TUXEDO Computers, who support free open source software and KDE development through KDE e.V.
